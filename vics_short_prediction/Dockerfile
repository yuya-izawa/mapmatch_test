FROM centos:centos7

RUN yum install -y \
        ccache \
        make \
        unzip \
        gdb \
        gcc \
        gcc-c++ \
        python3 \
        python3-pip \
        epel-release

RUN yum install -y --enablerepo=epel \
    nkf \
    cmake3 \
    cppcheck \
    ninja-build

#
# gcovr
#
ENV GCOVR_VERSION 4.1

RUN python3 -m pip install gcovr==${GCOVR_VERSION}

#
# pmccabe
#
RUN curl -fSL "https://github.com/datacom-teracom/pmccabe/archive/master.zip" > pmccabe.zip \
    && unzip pmccabe.zip \
    && cd pmccabe-master \
    && make install

#
# conan
#
ENV CONAN_USER_HOME /tmp
RUN pip3 install --upgrade setuptools
RUN pip3 install conan \
    && conan remote remove conan-center \
    && conan remote add artifactory http://conan.ntj.local/artifactory/api/conan/conan-local \
    && chmod ugo+rwX -R /tmp/.conan

# There settings should be placed at the last
# not to use ccache on building Docker image.
#
RUN ln -s /usr/local/bin/ccache /usr/local/bin/gcc \
    && ln -s /usr/local/bin/ccache /usr/local/bin/g++ \
    && ln -s /usr/local/bin/ccache /usr/local/bin/cc \
    && ln -s /usr/local/bin/ccache /usr/local/bin/c++

RUN yum clean all

CMD ["tail", "-f", "/dev/null"]
