#ifndef MAINNET_WRITER_H
#define MAINNET_WRITER_H

#include <cstdint>
#include <fstream>
#include <map>
#include <string>
#include <vector>

#include "mainnet_binary_format.h"
#include "mainnet_value_format.h"
#include "na2_wrapper.h"
#include "nni_wrapper.h"
#include "vics_access/vics_link.h"

/**
 * @enum データ判断フラグ
 */
enum TIA_DATA_FLG {
    FLAG_TIME_SLOT = 0,              //!< タイムスロット
    FLAG_TRAVEL_SPEED = 1,           //!< 旅行時間
    FLAG_TRAFFIC_JAM_LEVEL = 2,      //!< 渋滞度
    FLAG_REGULATION = 3,             //!< 規制
    FLAG_REGULATION_DETAILS = 4,     //!< 規制詳細
    FLAG_REGULATION_START_TIME = 5,  //!< 規制開始時刻
    FLAG_REGULATION_END_TIME = 6,    //!< 規制終了時刻
    FLAG_CAUSE = 7,                  //!< 原因事象
    FLAG_CAUSE_DETAILS = 8,          //!< 原因事象詳細
};

struct SecondMeshUnitInformation {
    mainnet::format::MeshInformation MeshInfo;  // メッシュ番号情報
    std::vector<mainnet::format::VicsLinkInformation>
            VicsLinkInfo;  // VICSリンク情報
};

/**
 * @brief The MLinkMeshInfomation class
 */
class MLinkMeshInfomation {
   public:
    //! 階層レベル
    uint32_t level_;
    //! メッシュ単位の情報
    std::vector<mainnet::format::MeshInformation> mesh_info_;
    //! Mリンク単位の情報
    std::vector<mainnet::format::MLinkInformation> link_info_;

   public:
    MLinkMeshInfomation() {}
    MLinkMeshInfomation(
            const uint32_t level,
            const std::vector<mainnet::format::MeshInformation>& meshes,
            const std::vector<mainnet::format::MLinkInformation>& links);
    MLinkMeshInfomation(const MLinkMeshInfomation& other)
            : level_(other.level_),
              mesh_info_(other.mesh_info_),
              link_info_(other.link_info_) {}

    void Write(std::ofstream& ofs) const;
};

class VicsMeshInfomation {
   public:
    //! 階層レベル
    const uint32_t level_;
    //! メッシュ単位の情報
    const std::vector<mainnet::format::MeshInformation> mesh_info_;
    //! VICSリンク単位の情報
    const std::vector<mainnet::format::VicsLinkInformation> vlink_info_;

   public:
    VicsMeshInfomation(
            const uint32_t level,
            const std::vector<mainnet::format::MeshInformation>& meshes,
            const std::vector<mainnet::format::VicsLinkInformation>& vlinks);

    void Write(std::ofstream& ofs) const;
};

class MainnetWriter {
   private:
    struct Headers {
        mainnet::format::VLinkHeader vlink_header;
        mainnet::format::AdditionalHeader additional;
    };

    Headers CreateHeaders();

    //! VICSリンク
    std::map<uint32_t, const VicsMeshInfomation> infos;
    //! SAPA
    std::vector<JAMINFO_SAPA_DEGREE> sapa_info_;
    //! Mリンク
    MLinkMeshInfomation minfos_;

    MainnetWriter(const MainnetWriter& other);

   public:
    MainnetWriter();
    ~MainnetWriter();

    void AddVicsMeshInfomation(
            const uint32_t level,
            const std::vector<mainnet::format::MeshInformation>& mesh_info,
            const std::vector<mainnet::format::VicsLinkInformation>&
                    vlink_info);

    void AddSapaInfo(const JAMINFO_SAPA_DEGREE* sapa, const uint32_t sapa_num);
    void AddMLinkMeshInfomation(const MLinkMeshInfomation& info) {
        this->minfos_ = info;
    }
    bool Write(const std::string& filename);
};

#endif  // MAINNET_WRITER_H

// 28Bytes
#ifndef _DEF_UNITED_HEADER_
#define _DEF_UNITED_HEADER_
typedef struct {
    uint32_t HeaderSize;    // ヘッダサイズ
    uint32_t TotalSize;     // 全データサイズ
    uint32_t Level;         // 階層レベル
    uint32_t Number;        // 総リンク数
    int8_t MapDate[8];      // 地図データの日付
    int8_t MapSupplier[4];  // 地図データの提供元
} UNITED_HEADER;
#endif

// 24Bytes
#ifndef _DEF_LINK_HEADER_
#define _DEF_LINK_HEADER_
struct LINK_HEADER {
    uint32_t sMesh;       // 連続メッシュ番号
    uint32_t Link;        // リンク番号
    uint32_t Number;      // VICSリンク数
    uint32_t SecondMesh;  // 2次メッシュ番号
    uint32_t VicsLink;    // VICSリンク番号
    uint32_t Address;  // データアドレス（ファイル先頭からのバイト数）

    LINK_HEADER()
            : sMesh(0),
              Link(0),
              Number(0),
              SecondMesh(0),
              VicsLink(0),
              Address(0) {}
};
#endif

// 8Bytes
#ifndef _DEF_VICS_LINK_INFO_
#define _DEF_VICS_LINK_INFO_
typedef struct {
    uint32_t SecondMesh;  // 2次メッシュ番号
    uint32_t VicsLink;    // VICSリンク番号
} VICS_LINK_INFO;
#endif

#ifndef _MF_LINK_
#define _MF_LINK_
struct MF_LINK {
    uint32_t dwMesh; /* メッシュ番号 */
    uint32_t dwLink; /* リンク番号 */

    MF_LINK() : dwMesh(0), dwLink(0) {}
    MF_LINK(const uint32_t mesh, const uint32_t link)
            : dwMesh(mesh), dwLink(link) {}

    bool operator==(const MF_LINK& other) const {
        return dwMesh == other.dwMesh && dwLink == other.dwLink;
    }
    bool operator!=(const MF_LINK& other) const { return !(*this == other); }
};
#endif