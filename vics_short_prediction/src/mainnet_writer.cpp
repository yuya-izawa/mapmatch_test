#include "mainnet_writer.h"

#include <algorithm>
#include <cassert>
#include <cstring>
#include <fstream>
#include <iostream>
#include <iterator>
#include <map>
#include <stdexcept>

// #include "mainnet_value_format.h"
// #include "mainnet_binary_format.h"
// #include "vics_access/vics_link.h"

using namespace mainnet::format;

std::ostream& operator<<(std::ostream& ofs, const JAMINFO_SAPA_DEGREE& sapa) {
    int8_t dummy[2] = {};
    //! @note 緯度経度の順に書き込む
    ofs.write(reinterpret_cast<const char*>(&sapa.reg.dw_nlat),
              sizeof(sapa.reg.dw_nlat));
    ofs.write(reinterpret_cast<const char*>(&sapa.reg.dw_nlog),
              sizeof(sapa.reg.dw_nlog));

    ofs.write(reinterpret_cast<const char*>(&sapa.degree), sizeof(sapa.degree));

    ofs.write(reinterpret_cast<const char*>(&sapa.large_degree),
              sizeof(sapa.large_degree));

    ofs.write(reinterpret_cast<const char*>(dummy), sizeof(dummy));
    return ofs;
}

MLinkMeshInfomation::MLinkMeshInfomation(
        const uint32_t level,
        const std::vector<mainnet::format::MeshInformation>& meshes,
        const std::vector<mainnet::format::MLinkInformation>& links)
        : level_(level), mesh_info_(meshes), link_info_(links) {}

/**
 * @brief MLinkMeshInfomation::Write
 * メッシュ情報部 -> リンク情報部の順に書き込む
 * @param[out] ofs
 */
void MLinkMeshInfomation::Write(std::ofstream& ofs) const {
    ofs.write(reinterpret_cast<const char*>(&this->mesh_info_.front()),
              sizeof(this->mesh_info_.front()) * this->mesh_info_.size());
    ofs.write(reinterpret_cast<const char*>(&this->link_info_.front()),
              sizeof(this->link_info_.front()) * this->link_info_.size());
}

/**
 * @brief VicsMeshInfomation::VicsMeshInfomation
 * @details Default Constructor
 * @param[in] level
 * @param[in] meshes
 * @param[in] vlinks
 */
VicsMeshInfomation::VicsMeshInfomation(
        const uint32_t level, const std::vector<MeshInformation>& meshes,
        const std::vector<VicsLinkInformation>& vlinks)
        : level_(level), mesh_info_(meshes), vlink_info_(vlinks) {}

/**
 * @brief VicsMeshInfomation::Write
 * メッシュ情報部 -> VICSリンク情報部の順に書き込む
 * @param[out] ofs
 */
void VicsMeshInfomation::Write(std::ofstream& ofs) const {
    ofs.write(reinterpret_cast<const char*>(&this->mesh_info_.front()),
              sizeof(MeshInformation) * this->mesh_info_.size());
    ofs.write(reinterpret_cast<const char*>(&this->vlink_info_.front()),
              sizeof(VicsLinkInformation) * this->vlink_info_.size());
}

MainnetWriter::MainnetWriter() {}

MainnetWriter::MainnetWriter(const MainnetWriter& /*other*/) {}

MainnetWriter::~MainnetWriter() {}

MainnetWriter::Headers MainnetWriter::CreateHeaders() {
    mainnet::format::VLinkHeader header;
    mainnet::format::AdditionalHeader additional_header;

    const std::size_t mesh_info_size = sizeof(MeshInformation);
    const std::size_t vlink_info_size = sizeof(VicsLinkInformation);

    //ヘッダ情報を作成
    memset(&header, 0, sizeof(VLinkHeader));
    memcpy((char*)header.identifier, mainnet::value::header_identifier.c_str(),
           mainnet::value::header_identifier.size());
    header.version = mainnet::value::version;

    uint32_t address = sizeof(VLinkHeader) + additional_header.header_size;

    // level2
    header.mesh2_address = address;
    header.mesh2_num = this->infos.at(2).mesh_info_.size();
    address += mesh_info_size * header.mesh2_num;

    header.vlink2_address = address;
    header.vlink2_num = this->infos.at(2).vlink_info_.size();
    address += vlink_info_size * header.vlink2_num;

    // level3
    header.mesh3_address = address;
    header.mesh3_num = this->infos.at(3).mesh_info_.size();
    address += mesh_info_size * header.mesh3_num;

    header.vlink3_address = address;
    header.vlink3_num = this->infos.at(3).vlink_info_.size();
    address += vlink_info_size * header.vlink3_num;

    // level4
    header.mesh4_address = address;
    header.mesh4_num = this->infos.at(4).mesh_info_.size();
    address += mesh_info_size * header.mesh4_num;

    header.vlink4_address = address;
    header.vlink4_num = this->infos.at(4).vlink_info_.size();
    address += vlink_info_size * header.vlink4_num;

    header.sapa_address = address;
    header.sapa_num = this->sapa_info_.size();

    header.additional_header_flag = true;

    // 追加ヘッダ部
    additional_header.mlink_reg_header_address =
            header.sapa_address +
            sizeof(this->sapa_info_.at(0)) * header.sapa_num;

    Headers headers;
    headers.vlink_header = std::move(header);
    headers.additional = std::move(additional_header);

    return std::move(headers);
}

/**
 * @brief MainnetWriter::AddVicsMeshInfomation
 * @details
 *  Add Arrays of VicsLinkInformation and MeshInformation to Writing
 * @param[in] level
 * @param[in] mesh_info
 * @param[in] vlink_info
 */
void MainnetWriter::AddVicsMeshInfomation(
        const uint32_t level, const std::vector<MeshInformation>& mesh_info,
        const std::vector<VicsLinkInformation>& vlink_info) {
    if (level < 2 || 4 < level) {
        throw std::runtime_error("Invalid mesh level numver :" +
                                 std::to_string(level));
    }

    const VicsMeshInfomation info(level, mesh_info, vlink_info);
    this->infos.insert(std::make_pair(level, info));
}

/**
 * @brief MainnetWriter::AddSapaInfo
 * @param[in] sapa
 * @param[in] sapa_num
 */
void MainnetWriter::AddSapaInfo(const JAMINFO_SAPA_DEGREE* sapa,
                                const uint32_t sapa_num) {
    this->sapa_info_.clear();
    this->sapa_info_.resize(sapa_num);
    this->sapa_info_.assign(sapa, sapa + sapa_num);
}

//=============================================================================
/*	@brief	渋滞情報を書き込む
 *	@param[in]	filename mainnetファイル出力名
 *	@return		BOOL値(true;成功、false：失敗)
 */
//=============================================================================
bool MainnetWriter::Write(const std::string& filename) {
    std::ofstream ofs(filename, std::ios_base::out | std::ios_base::binary |
                                        std::ios_base::trunc);
    if (!ofs) {
        throw std::runtime_error("Mainnet File open Error! :" + filename);
    }

    const Headers& header = CreateHeaders();
    // VICSメインネットのヘッダーを書き込む
    ofs.write(reinterpret_cast<const char*>(&header), sizeof(header));

    //メッシュ構造体書き込み
    for (const auto& info : this->infos) {
        info.second.Write(ofs);
    }

    // SA・PA情報を追加
    std::ostream_iterator<JAMINFO_SAPA_DEGREE> ofs_ite_sapa(ofs);
    std::copy(this->sapa_info_.begin(), this->sapa_info_.end(), ofs_ite_sapa);

    MeshLinkHeader level1_header;
    level1_header.mesh_address = header.additional.mlink_reg_header_address;
    level1_header.mesh_num = this->minfos_.mesh_info_.size();
    level1_header.link_address = level1_header.mesh_address +
                                 sizeof(this->minfos_.mesh_info_.at(0)) *
                                         this->minfos_.mesh_info_.size();
    level1_header.link_num = this->minfos_.link_info_.size();

    ofs.write(reinterpret_cast<const char*>(&level1_header),
              sizeof(level1_header));

    // Mリンク情報部を書き込み
    this->minfos_.Write(ofs);

    return true;
}