#ifndef NA2WRAPPER_H
#define NA2WRAPPER_H

#include <cstdint>
#include <memory>
#include <vector>

#include "netaccess2/NetAccess2_32.h"

namespace dao {

/**
 * @brief M-formatリンク
 */
class MformatLink {
   public:
    MformatLink(const uint32_t mesh_index, const uint32_t link_index)
            : mesh_index_(mesh_index), link_index_(link_index) {}
    uint32_t GetMeshIndex() const { return mesh_index_; }
    uint32_t GetLinkIndex() const { return link_index_; }

   private:
    //! メッシュ番号
    uint32_t mesh_index_;
    //! リンク番号
    uint32_t link_index_;
};

class Na2Wrapper {
   public:
    Na2Wrapper();
    explicit Na2Wrapper(const std::string& map_path);
    virtual ~Na2Wrapper();

    static std::vector<NA2_LOCATE_32_T> GetLinkShape(const NA2_INST& na2,
                                                     const MformatLink& link);
    virtual NA2_LINK_ATTR_32_T GetLinkAttr(const MformatLink& mlink) const;

    /**
     *	@brief	入力したmlinkの始点ノードから出るmlinksをすべて取得
     *	@param[in]	mesh	対象圧縮メッシュ
     *	@param[in]	link	対象mlink
     *	@param[out]	mlinks	始点ノードから出るmlinkのリンク列
     *	@param[out]	meshes	始点ノードから出るmlinkのメッシュ列
     *	@return		BOOL値	エラーフラグ true：取得成功, false：エラー発生
     */
    bool GetMlinksFromOrigin(const uint32_t mesh, const uint32_t link,
                             std::vector<uint32_t>* mlinks,
                             std::vector<uint32_t>* meshes);

    /**
     * @brief リンク種別による本線判定
     * @param リンク種別
     * @see NA2_LINK_ATTR_32_T.**a5**.type
     * @return boolean
     */
    static bool IsLinkTypeMainRoad(const uint32_t& attr) {
        return (attr == MFORMAT_LINK_TYPE_CAR_MAIN_ROAD_UNDIVIDED ||
                attr == MFORMAT_LINK_TYPE_CAR_MAIN_ROAD_DIVIDED);
    }

    /**
     * @brief 道路種別による高速道路判定
     * @param Mフォーマットの道路種別
     * @see NA2_LINK_ATTR_32_T.**a3**.mtype
     * @return boolean
     */
    static bool IsRoadTypeHighway(const uint32_t& attr) {
        return (attr == MFORMAT_ROAD_TYPE_CAR_NATIONAL_EXPRESSWAY ||
                attr == MFORMAT_ROAD_TYPE_CAR_URBAN_EXPRESSWAY);
    }

    /**
     * @brief IsVisibleWideScaleMap
     * クリッカブル渋滞線地図スケール1以上で表示する道路種別
     * - 1 : 高速道路国道
     * - 2 : 都市高速道路
     * - 3 : 一般国道
     * @param Mフォーマットの道路種別
     * @see NA2_LINK_ATTR_32_T.**a3**.mtype
     * @return boolean
     */
    static bool IsVisibleWideScaleMap(const uint32_t& attr) {
        return (IsRoadTypeHighway(attr) ||
                attr == MFORMAT_ROAD_TYPE_CAR_NATIONAL);
    }

   private:
    std::unique_ptr<NA2_INST> na2_;
};

}  // namespace dao
#endif  // NA2WRAPPER_H
