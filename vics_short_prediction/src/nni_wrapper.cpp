#include "nni_wrapper.h"

#include <cassert>
#include <cstdint>
#include <iostream>
#include <sstream>
#include <stdexcept>

#include "commonlibrary.h"
#include "vics_access/vics_link.h"

#include "na2_wrapper.h"

namespace dao {

namespace {
static uint32_t kNetInfoMemoryMax = 4 * 1024 * 1024;
static const uint32_t kLinkBuffSize = 256;
}  // namespace

/**
 * @brief NniWrapper::NniWrapper
 */
NniWrapper::NniWrapper() : instance_(nullptr) {}

NniWrapper::NniWrapper(const std::string& map_path) : instance_(nullptr) {
    this->Initialize(map_path);
}

/**
 * @brief NniWrapper::~NniWrapper
 */
NniWrapper::~NniWrapper() {
    if (this->instance_ != nullptr) {
        spmNetInfoExit_r(&this->instance_);
    }
}

/**
 * @brief NniWrapper::Initialize
 * @note すでにNewNetInfoが初期化されていた場合は破棄して再構築する
 * @param map_path 地図データパス
 * @return 失敗は非ゼロ
 */
uint32_t NniWrapper::Initialize(const std::string& map_path) {
    if (this->instance_ != nullptr) {
        spmNetInfoExit_r(&this->instance_);
        this->instance_ = nullptr;
    }
    const auto ret = spmNetInfoInit_r(&this->instance_, map_path.c_str(),
                                      kNetInfoMemoryMax);
    if (ret != NETINFO_SUCCESS) {
        std::cerr << "[NniWrapper::Initialize] spmNetInfoGetVersion_r feild! "
                     "ret:" + ret
                  << " map_path:" + map_path << std::endl;
        return ret;
    }
    return 0;
}

/**
 * @brief NniWrapper::GetLibraryVersion
 * @return NewNetInfoのライブラリバージョン
 */
std::string NniWrapper::GetLibraryVersion() {
    assert(this->instance_ != nullptr);
    NETINFO_INFORMATION info;
    const auto ret = spmNetInfoGetVersion_r(this->instance_, &info);
    if (ret != NETINFO_SUCCESS) {
        throw std::runtime_error("spmNetInfoGetVersion_r feild! ret:" +
                                 std::to_string(ret));
    }
    return std::string(info.netinfo_version);
}

/**
 * @brief NniWrapper::GetMformatLinkFromVicsLinkId
 * @details TLBのV83ファイルから取得する
 * @param vlink
 * @return mlink列
 */
std::vector<MformatLink> NniWrapper::GetMformatLinkFromVicsLinkId(
        const VicsLink& vlink) const {
    assert(this->instance_ != nullptr);

    /* VICSリンクの2次メッシュから4分割した左下の圧縮メッシュを取得 */
    const uint32_t bmesh =
            common::mesh::GetBmeshFrom2ndMesh(vlink.GetSecondMesh());

    // vlinkをmLinkに変換
    uint32_t mlink_count = kLinkBuffSize;
    uint32_t bmesh_buf[kLinkBuffSize];
    std::fill_n(bmesh_buf, kLinkBuffSize, 0);
    uint32_t mlink_buf[kLinkBuffSize];
    std::fill_n(mlink_buf, kLinkBuffSize, 0);

    /* レベル1Mリンク列を取得する */
    const int nif_ret = spmGetV83MformatLinkIDFromVicsLinkID_r(
            this->instance_, bmesh, vlink.GetIntegrationNumber(), bmesh_buf,
            mlink_buf, &mlink_count);

    if (nif_ret == NETINFO_CANNOTFIND_ERROR) {
        return std::vector<MformatLink>();
    }

    if (nif_ret != NETINFO_SUCCESS) {
        std::stringstream ss;
        ss << __FUNCTION__ << ","
           << "spmGetV83MformatLinkIDFromVicsLinkID_r() ret : " << nif_ret
           << " second_mesh = " << vlink.GetSecondMesh() << ",bmesh = " << bmesh
           << ",vlink = " << vlink.GetIntegrationNumber();
        throw std::invalid_argument(ss.str());
    }

    // メッシュ境界をまたぐためメッシュ番号とペアで返却する
    std::vector<MformatLink> mlinks;
    for (uint32_t i = 0; i < mlink_count; ++i) {
        mlinks.push_back(MformatLink(bmesh_buf[i], mlink_buf[i]));
    }

    return std::move(mlinks);
}

}  // namespace dao
