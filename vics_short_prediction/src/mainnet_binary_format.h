#ifndef __MAINNET_BINARY_FORMAT_H__
#define __MAINNET_BINARY_FORMAT_H__

#include <cstdint>

#pragma pack(push, 4)

// 緯度経度構造体
#ifndef _DEF_REGULAR_T_
#define _DEF_REGULAR_T_
struct REGULAR_T {
    int32_t dw_nlog;  // 経度
    int32_t dw_nlat;  // 緯度

    REGULAR_T() : dw_nlog(0), dw_nlat(0) {}
};
#endif

// SA・PA満空情報構造体
#ifndef _JAMINFO_SAPA_DEGREE_T_
#define _JAMINFO_SAPA_DEGREE_T_
struct JAMINFO_SAPA_DEGREE {
    REGULAR_T reg;  //位置情報
    int8_t degree;  //満空情報　０空　１混雑　２満車　３閉鎖
    int8_t large_degree;  //大型車向け満空情報　０空　１混雑　２満車　３閉鎖　７不明

    JAMINFO_SAPA_DEGREE() : reg(), degree(0) {}
};
#endif

namespace mainnet {

namespace value {

namespace enable_flags {
//! フィールド有効フラグ(VicsLinkInformationのインデックス)
enum FieldEnableFlags {
    kTimeSlotIntermit = 0,  //!< 規制時間帯種別
    kUseTravelSpeed = 1,    //!< 速度フィールドが有効
    kUseJam = 2,            //!< 渋滞度フィールドが有効
    kUseRegulation = 3,     //!< 規制内容フィールドが有効
    kUseRegDetail = 4,      //!< 規制内容詳細フィールドが有効
    kUseStartTime = 5,      //!< 規制開始日時フィールドが有効
    kUseEndTime = 6,        //!< 規制終了日時フィールドが有効
    kUseCause = 7,          //!< 原因事象フィールドが有効
    kUseCauseDetail = 8,    //!< 原因事象詳細フィールドが有効
    kFlagsNum = 9
};
}  // namespace enable_flags
}  // namespace value

namespace format {

struct VICS_LINK {
    uint32_t dwMesh;  //!< メッシュ番号
    uint8_t cLayer;   //!< リンクレイヤ
    uint8_t cDiv;     //!< リンク区分
    uint16_t wVLink;  //!< VICSリンク番号

    VICS_LINK() : dwMesh(0), cLayer(0), cDiv(0), wVLink(0) {
        static_assert(sizeof(*this) == 0x08, "Unexpected struct size");
    }
    VICS_LINK(const VICS_LINK& other) {
        this->dwMesh = other.dwMesh;
        this->cLayer = other.cLayer;
        this->cDiv = other.cDiv;
        this->wVLink = other.wVLink;
    }
    VICS_LINK(const uint32_t mesh, const uint8_t layer, const uint8_t div,
              const uint16_t link)
            : dwMesh(mesh), cLayer(layer), cDiv(div), wVLink(link) {}

    VICS_LINK(const uint32_t mesh, const uint32_t vics_link_id) : dwMesh(mesh) {
        SetVicsLinkId(vics_link_id);
    }

    ~VICS_LINK() {}

    bool operator==(const VICS_LINK& other) const {
        return this->dwMesh == other.dwMesh &&
               this->GetCombinedVicsId() == other.GetCombinedVicsId();
    }
    bool operator!=(const VICS_LINK& other) const { return !(*this == other); }

    bool operator<(const VICS_LINK& other) const {
        if (this->dwMesh != other.dwMesh) {
            return this->dwMesh < other.dwMesh;
        }
        return this->GetCombinedVicsId() < other.GetCombinedVicsId();
    }

    void SetVicsLinkId(const uint32_t vics_link) {
        this->cLayer = (vics_link & 0xc000) >> 0x0E;
        this->cDiv = (vics_link & 0x3000) >> 0x0C;
        this->wVLink = (vics_link & 0x0fff);
    }
    uint16_t GetCombinedVicsId() const {
        uint16_t vics_link_id = 0;
        vics_link_id |= this->cLayer << 0x0E;
        vics_link_id |= this->cDiv << 0x0C;
        vics_link_id |= this->wVLink;

        return vics_link_id;
    }
};

//! メッシュ情報
struct MeshInformation {
    uint32_t mesh;  //!< メッシュ番号
    uint32_t size;  //!< リンク数

    MeshInformation() : mesh(0), size(0) {
        static_assert(sizeof(*this) == 0x08, "Unexpected struct size");
    }
    MeshInformation(const MeshInformation& other) {
        this->mesh = other.mesh;
        this->size = other.size;
    }
    ~MeshInformation() {}
};

//! VICSリンク情報
struct VicsLinkInformation {
    VICS_LINK link;                                //!< VICSリンクID
    uint32_t speed;                                //!< 速度[cm/s]
    uint32_t reg_start_time;                       //!< 規制開始日時
    uint32_t reg_end_time;                         //!< 規制終了日時
    uint16_t jam_start_pos;                        //!< 渋滞開始位置[10m]
    uint16_t jam_end_pos;                          //!< 渋滞終了位置[10m]
    uint16_t reg_start_pos;                        //!< 規制開始位置[10m]
    uint16_t reg_end_pos;                          //!< 規制終了位置[10m]
    uint8_t jam_lv;                                //!< 渋滞度
    uint8_t reg;                                   //!< 規制内容
    uint8_t cause;                                 //!< 原因事象
    uint8_t detail_reg;                            //!< 規制内容詳細
    uint8_t detail_cause;                          //!< 原因事象詳細
    uint8_t flag[value::enable_flags::kFlagsNum];  //!< フィールド有効フラグ
    uint8_t reserved[6];                           //!< 予約値

    VicsLinkInformation()
            : link(),
              speed(0),
              reg_start_time(0),
              reg_end_time(0),
              jam_start_pos(0),
              jam_end_pos(0),
              reg_start_pos(0),
              reg_end_pos(0),
              jam_lv(0),
              reg(0),
              cause(0),
              detail_reg(0),
              detail_cause(0),
              flag(),
              reserved() {
        static_assert(sizeof(*this) == 0x30, "Unexpected struct size");
    }

    bool operator<(const VicsLinkInformation& other) const {
        return link < other.link;
    }

    bool operator==(const VicsLinkInformation& other) const {
        return link == other.link;
    }
};

/* ヘッダ要素 64B */
#define MADE_DAY_LENGTH 14  //!< YYYYMMDDHHNNSS
struct Mainnet_HeaderElement {
    int8_t id[2];          //!< Mainnet 'MH'
    uint16_t header_size;  //!< ヘッダのサイズ(64) 固定
    int8_t dum1[4];
    int8_t filever[8];                      //!< Mainnet Version 001.00
    int8_t data_made_day[MADE_DAY_LENGTH];  //!< 元データ作成日 YYYYMMDDHHNNSS
    int8_t data_made_company[4];            //!< 元データ作成会社 VICS
    int8_t dum2[30];

    Mainnet_HeaderElement()
            : id(),
              header_size(0x40),
              dum1(),
              filever(),
              data_made_day(),
              data_made_company(),
              dum2() {
        static_assert(sizeof(*this) == 0x40, "Unexpected struct size");
    }
};

//! メインネットファイルのメッシュ・リンクヘッダー情報構造体型
struct MeshLinkHeader {
    uint32_t mesh_address;  //!< メッシュ情報へのアドレス
    uint32_t mesh_num;      //!< メッシュ情報の総数
    uint32_t link_address;  //!< リンク情報へのアドレス
    uint32_t link_num;      //!< リンク情報の総数

    MeshLinkHeader()
            : mesh_address(0), mesh_num(0), link_address(0), link_num(0) {
        static_assert(sizeof(*this) == 0x10, "Unexpected struct size");
    }
};

struct VLinkHeader {
    int8_t identifier[4];  //!< 識別子

    uint32_t mesh2_address;  //!< レベル2のメッシュ情報へのアドレス
    uint32_t mesh2_num;      //!< レベル2のメッシュ情報の総数
    uint32_t vlink2_address;  //!< レベル2のリンク情報へのアドレス
    uint32_t vlink2_num;      //!< レベル2のリンク情報の総数

    uint32_t mesh3_address;  //!< レベル3のメッシュ情報へのアドレス
    uint32_t mesh3_num;      //!< レベル3のメッシュ情報の総数
    uint32_t vlink3_address;  //!< レベル3のリンク情報へのアドレス
    uint32_t vlink3_num;      //!< レベル3のリンク情報の総数

    uint32_t mesh4_address;  //!< レベル4のメッシュ情報へのアドレス
    uint32_t mesh4_num;      //!< レベル4のメッシュ情報の総数
    uint32_t vlink4_address;  //!< レベル4のリンク情報へのアドレス
    uint32_t vlink4_num;      //!< レベル4のリンク情報の総数

    uint32_t sapa_address;  //!< SAPA情報へのアドレス
    uint32_t sapa_num;      //!< SAPA情報の総数
    uint8_t version;        //!< mainnetフォーマットバージョン
    int8_t additional_header_flag;  //!< 追加ヘッダー存在フラグ
    int8_t dummy[2];                //!< 予備

    VLinkHeader()
            : identifier(),
              mesh2_address(0),
              mesh2_num(0),
              vlink2_address(0),
              vlink2_num(0),
              mesh3_address(0),
              mesh3_num(0),
              vlink3_address(0),
              vlink3_num(0),
              mesh4_address(0),
              mesh4_num(0),
              vlink4_address(0),
              vlink4_num(0),
              sapa_address(0),
              sapa_num(0),
              version(0),
              additional_header_flag(false),
              dummy() {
        static_assert(sizeof(*this) == 0x40, "Unexpected struct size");
    }
};  //!< ヘッダー情報  64byte

struct AdditionalHeader {
    uint32_t header_size;               //!< 追加ヘッダー部サイズ
    uint32_t mlink_reg_header_address;  //!< Mリンク規制情報ヘッダ部アドレス
    uint8_t reserve[56];                //!< 予備領域

    AdditionalHeader()
            : header_size(64), mlink_reg_header_address(0), reserve() {
        static_assert(sizeof(*this) == 0x40, "Unexpected struct size");
    }
};

struct MLinkInformation {
    uint32_t link;
    uint32_t reserve;
    MLinkInformation() : link(0), reserve(0) {
        static_assert(sizeof(*this) == 0x08, "Unexpected struct size");
    }
    ~MLinkInformation() {}
    explicit MLinkInformation(const uint32_t l) : link(l), reserve() {}
    explicit MLinkInformation(const MLinkInformation& other)
            : link(other.link), reserve(other.reserve) {}
};

}  // namespace format
}  // namespace mainnet
#pragma pack(pop)

#endif  // __MAINNET_BINARY_FORMAT_H__
