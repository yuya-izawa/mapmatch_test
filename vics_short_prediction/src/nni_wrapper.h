#ifndef NNIWRAPPER_H
#define NNIWRAPPER_H

#include <cstdint>
#include <memory>
#include <string>
#include <vector>

#include "NewNetInfo/NetInfo.h"

// from vicsaccess
class VicsLink;

namespace dao {

class MformatLink;

class NniWrapper {
   public:
    NniWrapper();
    explicit NniWrapper(const std::string& map_path);
    virtual ~NniWrapper();

    virtual uint32_t Initialize(const std::string& map_path);
    virtual std::string GetLibraryVersion();
    virtual std::vector<MformatLink> GetMformatLinkFromVicsLinkId(
            const VicsLink& vlink) const;

   private:
    NniWrapper(const NniWrapper& other);

    NIF_IODEF_INSTANCE instance_;
    std::string map_path_;
};

}  // namespace dao

#endif  // NNIWRAPPER_H
