#include <iostream>

#include "vics_short_prediction.hpp"

namespace util {
enum class EReturnCode {
    kSuccess = 0,
    kArgumentError = 1,
    kConvertError = 2,
    kOutputFileError = 2,
};
}  // namespace util

using namespace util;

int main(int argc, char* argv[]) {
    if (argc != 6) {
        std::cerr << "[WARNING] input 1: mainnet file" << std::endl;
        std::cerr << "[WARNING] input 2: unixtime" << std::endl;
        std::cerr << "[WARNING] input 3: ppd path" << std::endl;
        std::cerr << "[WARNING] input 4: map path" << std::endl;
        std::cerr << "[WARNING] input 5: output dir" << std::endl;
        return static_cast<int>(EReturnCode::kArgumentError);
    }

    std::string mainnet_path = argv[1];
    std::string ppd_dir = argv[3];
    std::string map_path = argv[4];
    std::string output_dir = argv[5];

    time_t basetime = 0;
    try {
        basetime = std::stol(argv[2]);
    } catch (const std::invalid_argument& e) {
        std::cerr << "[Warning] invalid argument! basetime(argv[5]): "
                  << argv[5] << std::endl;
    } catch (const std::out_of_range& e) {
        std::cerr << "[Warning] invalid argument! basetime(argv[5]): "
                  << argv[5] << std::endl;
    }

    VICSShortPrediction vics_short_prediction(basetime);
    if (!vics_short_prediction.Prediction(mainnet_path, ppd_dir, map_path)) {
        return static_cast<int>(EReturnCode::kConvertError);
    }

    if (!vics_short_prediction.CreateMainnet(output_dir, map_path)) {
        return static_cast<int>(EReturnCode::kOutputFileError);
    }

    if (!vics_short_prediction.CreateSphed(output_dir)) {
        return static_cast<int>(EReturnCode::kOutputFileError);
    }

    return static_cast<int>(EReturnCode::kSuccess);
}
