#ifndef VICS_SHORT_PREDICTION_H
#define VICS_SHORT_PREDICTION_H

#include <vector>
#include "link_manager.hpp"

#include "ProbeCommonLibrary/probe_code_scheme.h"
#include "ProbeCommonLibrary/probe_netaccess_struct.h"
/**
 * @brief　渋滞発生時間と渋滞収束時間のペア
 * pair<start_time, end_time>
 */
typedef std::pair<time_t, time_t> JAM_PERIOD;

/**
 * @brief　速度と渋滞度のペア
 * pair<speed, traffic_lv>
 */
typedef std::pair<uint32_t, TRAFFIC_LV> SPEED;

struct InputData {
    LINK_ATTR_T link_attr;
    uint32_t standard_speed;
    uint32_t min_speed;
    std::vector<uint32_t> ppd_speed_list;
    std::vector<uint32_t> peak_speed;
    std::vector<JAM_PERIOD> jam_period_list;
    std::vector<time_t> jam_peak_time_list;
};

class VICSShortPrediction {
    public:
        VICSShortPrediction(const time_t& basetime);
        bool Prediction(const std::string& mainnet_path, const std::string& ppd_path, const std::string& map_path);
        bool ConvertInputData(const uint32_t& b_mesh, const uint16_t& link, const std::string& ppd_path, const std::string& map_path, InputData* input) const;
        bool ComplementSpeed(const uint32_t& current_speed, const InputData& input, std::vector<SPEED>* speed_list) const;
        bool IsPredictionTarget(const uint32_t& mesh,const uint32_t& link,const time_t& basetime);
        void InitLinkManager();
        void SetLinkManager(const uint32_t& mesh, const uint32_t& vics_link, const std::vector<SPEED>& speed_list);
        bool SetMainnetPath(const std::string& mainnet_dir);
        bool CreateMainnet(const std::string& output_directory,
                       const std::string& map_path);
        bool CreateSphed(const std::string& output_directory) const;
        //void GetLinkManagerMap(std::map<uint32_t, LinkManager>* link_manager_map);

        
    private:
        time_t basetime_;
        struct tm base_time_tm_;
        std::vector<time_t> today_time_list_;
        std::map<uint32_t, LinkManager> link_manager_map_;
        std::vector<std::string> mainnet_name_list_;
};

#endif //VICS_SHORT_PREDICTION_H
