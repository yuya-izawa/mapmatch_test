#include "vics_short_prediction.hpp"

#include <iostream>
#include <cmath>
#include <sstream>
#include <algorithm>
#include <iomanip>
#include <time.h>
#include <vector>
//debug用
#include <fstream>

#include "mainnetaccess.h"
#include "ppdaccess/ppdaccess.h"
#include "commonlibrary.h"

#include "netaccess2/NetAccess2.h"
#include "ProbeCommonLibrary/probe_netaccess_car.hpp"
#include "ProbeCommonLibrary/traffic_info_func.hpp"
#include "mainnet_binary_format.h"

using namespace mainnet::value::enable_flags;

const std::string kMainnetHed = "mainnet.hed";
constexpr uint32_t kTimeSpan = 1800;
constexpr uint32_t kMaxLinkCount = 256;
constexpr uint32_t kTargetHour = 6;
constexpr uint32_t kMaxFileCount = (kTargetHour * 60 * 60) / kTimeSpan;

namespace util {
inline std::string MakeDataPrefix(const time_t& dep_time) {
    tm tm;
    localtime_r(&dep_time, &tm);

    // fix for filename

    std::stringstream ss;
    ss << "20" << tm.tm_year - 100;
    // setw(),setfill()で0詰め
    ss << setw(2) << setfill('0') << tm.tm_mon + 1;
    ss << setw(2) << setfill('0') << tm.tm_mday;
    ss << setw(2) << setfill('0') << tm.tm_hour;
    ss << setw(2) << setfill('0') << tm.tm_min;
    ss << setw(2) << setfill('0') << "0";

    return ss.str();  
}
inline double CalcHarmonicMean(const std::vector<uint32_t> speed_list) {
    double average = 0;
    try {
        for (const auto& speed : speed_list) {
            average += static_cast<double>(1.0 / speed);
        }
        average = speed_list.size() / average;
    } catch(std::logic_error e) {
        std::cerr << e.what();
        return 0;
    }
    return average;
}

int GetCombinedVicsId(int traffic_layer, int traffic_type, int traffic_id) {
    int vics_link_id = 0;
    vics_link_id |= traffic_layer << 0x0E;
    vics_link_id |= traffic_type << 0x0C;
    vics_link_id |= traffic_id;
    return vics_link_id;
}
}  // namespace util

VICSShortPrediction::VICSShortPrediction(const time_t& basetime)
    : basetime_(basetime) {
        InitLinkManager();

        struct tm base_tm = {0}; 
        localtime_r(&basetime_, &base_tm);
        base_time_tm_ = base_tm;

        struct tm today_tm = base_time_tm_; 
        today_tm.tm_hour = 0;
        today_tm.tm_min = 0;
        today_tm.tm_sec = 0;
        time_t target_time = mktime(&today_tm);
        //15分刻みでアクセスする 
        for (int i = 0; i < (24 * 3600 / kTimeSpan); ++i) {
            today_time_list_.emplace_back(target_time + kTimeSpan * i);
        }
    }

bool VICSShortPrediction::Prediction(const std::string& mainnet_path, const std::string& ppd_path, const std::string& map_path) {
    NA2_INST inst;
    int ret = na2InitNetAccess_r(&inst, NA2_MODE_NET_ACCESS_FULL, map_path.c_str());
    if (ret != NA2_SUCCESS) {
        std::cerr << "[ERROR] na2InitNetAccess_r. ret: " << ret << std::endl;
        na2ExitNetAccess_r(&inst);
        return false;
    }
    ret = na2SetMeshFormat_r(inst, NA2_MESH_FORMAT_SERIAL);
    if (ret != NA2_SUCCESS) {
        std::cerr << "[ERROR] na2SetMeshFormat_r. ret: " << ret << std::endl;
        na2ExitNetAccess_r(&inst);
        return false;
    }
    NA2_ELEMENT_HEADER_T e_hdr;
    ret = na2GetElementHeader_r(inst, &e_hdr);
    if (ret != NA2_SUCCESS) {
        std::cerr << "[ERROR] na2GetElementHeader_r. ret: " << ret << std::endl;
        na2ExitNetAccess_r(&inst);
        return false;
    }
   
    MNA_HANDLE mna_handle = {0};
    int mna_ret = MNA_CreateHandle(mainnet_path.c_str(), &mna_handle);
    if (mna_ret != MNA_SUCCESS) {
        std::cerr << "[ERROR] MNA_CreateHandle. ret: " << ret << std::endl;
        na2ExitNetAccess_r(&inst);
        return false;
    }

    for (int s_mesh = 0; s_mesh < e_hdr.mesh_count_lv1; ++s_mesh) {
        unsigned long b_mesh;
        ret = na2GetSMeshToBMesh_r(inst, s_mesh, &b_mesh);
        if (ret != NA2_SUCCESS) {
            std::cerr << "[ERROR] na2GetSMeshToBMesh_r. ret: " << ret << std::endl;
            na2ExitNetAccess_r(&inst);
            MNA_DestroyHandle(&mna_handle);
            return false;
        }
        NA2_NET_HEADER_T n_hdr;
        ret = na2GetNetHeader_r(inst, 2, s_mesh, &n_hdr);
        if (ret != NA2_SUCCESS) {
            std::cerr << "[ERROR] na2GetNetHeader_r. ret: " << ret << std::endl;
            na2ExitNetAccess_r(&inst);
            MNA_DestroyHandle(&mna_handle);
            return false;
        }
        for (int link = 0; link < n_hdr.link_su; ++link) {
            NA2_LINK_LV2_T l_data;
            ret = na2GetLinkData_r(inst, 2, s_mesh, link, &l_data);
            if (ret != NA2_SUCCESS) {
                std::cerr << "[ERROR] na2GetLinkData_r. ret: " << ret << std::endl;
                na2ExitNetAccess_r(&inst);
                MNA_DestroyHandle(&mna_handle);
                return false;
            }
            if (l_data.traffic_id == 0) {
                continue;
            }
            unsigned long n_mesh = 0;
            ret = na2GetBMeshToNMesh(b_mesh, &n_mesh);
            if (ret != NA2_SUCCESS) {
                std::cerr << "[ERROR] na2GetBMeshToNMesh. ret: " << ret << std::endl;
                na2ExitNetAccess_r(&inst);
                MNA_DestroyHandle(&mna_handle);
                return false;
            }
            int vics_link_id = util::GetCombinedVicsId(l_data.traffic_laye, l_data.traffic_type, l_data.traffic_id);
            MNA_TRAFFIC_INFO traffic_info = {0};
            mna_ret = MNA_GetTrafficInfo(mna_handle, 2, n_mesh / 100, vics_link_id, &traffic_info);
            if (mna_ret != MNA_SUCCESS) {
                std::cerr << "[ERROR] MNA_GetTrafficInfo. ret: " << ret << std::endl;
                na2ExitNetAccess_r(&inst);
                MNA_DestroyHandle(&mna_handle);
                return 1;
            }
            // if (!(b_mesh == 1579339281 && link == 2)) {
            //         //std::cout << "アクアライン！" << std::endl;
            //         continue;
            // }
            if (traffic_info.speed == 0) {
                continue;
            }
            // 渋滞が配信されたリンクのみ予測対象にする
            if (traffic_info.traffic_lv == 3 ) {
                uint32_t speed = traffic_info.speed;
                InputData input;
                if (!ConvertInputData(b_mesh, vics_link_id, ppd_path, map_path, &input)) {
                    continue;
                }
                std::vector<SPEED> speed_list;
                if (ComplementSpeed(speed, input, &speed_list)) {
                    SetLinkManager(n_mesh / 100,vics_link_id, speed_list);
                }
                //デバッグ用CSV出力
                // std::ofstream output_csv("speed.csv");
                // for (const auto& speed : speed_list) {
                //     output_csv << speed.first << std::endl;
                // }
            }
        }
    }
    MNA_DestroyHandle(&mna_handle);
    return true;
}

/**
 * @brief mainnetを出力する 
 * 
 * @param output_directory
 * @param map_path 
 * @return true 
 * @return false 
 */
bool VICSShortPrediction::CreateMainnet(const std::string& output_directory, const std::string& map_path) {
    for (auto& link_manager : link_manager_map_) {
        std::stringstream ss;
        ss << std::setw(2) << std::setfill('0')
           << std::to_string(link_manager.first);
        std::string mainnet_file = output_directory + "/" +
                                   to_string(basetime_) + "_" + ss.str() +
                                   ".mainnet";
        MainnetWriter mainnet_writer;
        // lv2のmesh_infoを作成
        link_manager.second.CreateMeshInfo(2);
        //階層レベル3と4を作成するため階層レベル2を変換する
        std::vector<SecondMeshUnitInformation> secondmesh_info;
        std::vector<mainnet::format::MeshInformation> mesh_info_lv2;
        if (link_manager.second.GetMeshInfo(2, &mesh_info_lv2) == false) {
            continue;
        }
        std::vector<mainnet::format::VicsLinkInformation> vics_link_info_lv2;
        if (link_manager.second.GetVICSLinkInfo(2, &vics_link_info_lv2) ==
            false) {
            continue;
        }
        uint32_t idx = 0;
        for (const auto& info : mesh_info_lv2) {
            SecondMeshUnitInformation secondmesh;
            secondmesh.MeshInfo = info;

            const auto& begin = vics_link_info_lv2.begin() + idx;
            std::copy_n(begin, secondmesh.MeshInfo.size,
                        std::back_inserter(secondmesh.VicsLinkInfo));
            idx += secondmesh.MeshInfo.size;

            secondmesh_info.push_back(secondmesh);
        }

        if (link_manager.second.GenerateVICSLinkInfo(
                    map_path, 3, secondmesh_info) == false) {
            continue;
        }

        if (link_manager.second.GenerateVICSLinkInfo(
                    map_path, 4, secondmesh_info) == false) {
            continue;
        }

        std::vector<mainnet::format::MeshInformation> mesh_info_lv3;
        if (link_manager.second.GetMeshInfo(3, &mesh_info_lv3) == false) {
            continue;
        }
        std::vector<mainnet::format::MeshInformation> mesh_info_lv4;
        if (link_manager.second.GetMeshInfo(4, &mesh_info_lv4) == false) {
            continue;
        }

        std::vector<mainnet::format::VicsLinkInformation> vics_link_info_lv3;
        if (link_manager.second.GetVICSLinkInfo(3, &vics_link_info_lv3) ==
            false) {
            continue;
        }
        std::vector<mainnet::format::VicsLinkInformation> vics_link_info_lv4;
        if (link_manager.second.GetVICSLinkInfo(4, &vics_link_info_lv4) ==
            false) {
            continue;
        }

        mainnet_writer.AddVicsMeshInfomation(2, mesh_info_lv2,
                                             vics_link_info_lv2);
        mainnet_writer.AddVicsMeshInfomation(3, mesh_info_lv3,
                                             vics_link_info_lv3);
        mainnet_writer.AddVicsMeshInfomation(4, mesh_info_lv4,
                                             vics_link_info_lv4);
        mainnet_writer.Write(mainnet_file);
    }
    return true;
}

/**
 * @brief vics短期のヘッダファイルを作成する
 *
 * @param output_directory
 * @return bool
 */
bool VICSShortPrediction::CreateSphed(const std::string& output_directory) const {
    std::string header_path =
            output_directory + "/" + to_string(basetime_) + ".sphed";
    std::ofstream output_csv(header_path.c_str());
    if (output_csv.fail()) {
        std::cout << "write header failed!(path:" << header_path << ")"
                  << std::endl;
        return false;
    }
    output_csv << "mainnet"
               << "," << basetime_ << "," << std::to_string(kTimeSpan) << ","
               << std::to_string(kMaxFileCount) << std::endl;
    return true;
}

bool VICSShortPrediction::ConvertInputData(const uint32_t& b_mesh, const uint16_t& vics_link, const std::string& ppd_path, const std::string& map_path, InputData* input) const {
    NetAccessCar netaccess;
    PROBE_RC_T na_ret = netaccess.Init(map_path);
    if (na_ret != PROBE_RC_OK) {
        std::cerr << "[ERROR] netaccess_Init. ret: " << na_ret << std::endl;
        return false;
    }

    NIF_IODEF_INSTANCE nif_inst = nullptr;
    int32_t nif_ret =
            spmNetInfoInit_r(&nif_inst, map_path.c_str(), 100 * 1024 * 1024);
    if (nif_ret != NETINFO_SUCCESS) {
        std::cerr << "[ERROR] spmNetInfoInit_r. ret: " << nif_ret << std::endl;
        netaccess.Exit();
        return false;
    }
    std::vector<uint32_t> mformat_mesh(kMaxLinkCount, 0);
    std::vector<uint32_t> mformat_link(kMaxLinkCount, 0);
    uint32_t mformat_link_count = 0;
    nif_ret = spmGetV83MformatLinkIDFromVicsLinkID_r(
            nif_inst, b_mesh, vics_link,
            &mformat_mesh[0], &mformat_link[0],
            &mformat_link_count);
    if (nif_ret == NETINFO_CANNOTFIND_ERROR ||
        mformat_link_count == 0) {
        return true;
    }
    if (nif_ret != NETINFO_SUCCESS) {
        std::cerr << "[ERROR] "
                    "spmGetV83MformatLinkIDFromVicsLinkID_r. "
                    "ret: "
                << nif_ret << std::endl;
        netaccess.Exit();
        spmNetInfoExit_r(&nif_inst);
        return false;
    }
    LINK_ATTR_T link_attr = {0};
    // 最初のmlinkを使用
    na_ret = netaccess.GetLinkAttribute(
            mformat_mesh[0], mformat_link[0], link_attr);
    if (na_ret != PROBE_RC_OK) {
        std::cerr << "[ERROR] GetLinkAttribute. ret: " << na_ret
                << std::endl;
        netaccess.Exit();
        spmNetInfoExit_r(&nif_inst);
        return false;
    }
    //高速のみ適用してみる
    if (link_attr.road_type > 2) {
        netaccess.Exit();
        spmNetInfoExit_r(&nif_inst);
        return false;
    }

    input->link_attr = link_attr;

    PPA_HANDLE ppa_handle = {0};
    PPA_RET ppa_ret = ppa_createHandle(&ppa_handle, ppd_path.c_str());
    if (ppa_ret != PPA_RET_SUCCESS) {
        std::cerr << "Failed ppa_createHandle! RET: " << ppa_ret << std::endl;
    }
    PPA_INPUT ppa_input = {0};
    ppa_input.bmesh = b_mesh;
    // TODO とりあえず先頭のリンクでアクセスするがあとで要確認
    ppa_input.mlink = mformat_link[0];
    ppa_input.slv = PPA_SEARCH_LV_1;
    
    input->ppd_speed_list.clear();
    for (const auto& target_time : today_time_list_) {
        // kTimeSpanごとにデータにアクセスする
        ppa_input.time = target_time;
        PPA_DATA data = {0};
        ppa_ret = ppa_getPredictData(ppa_handle, &ppa_input, &data);
        if(ppa_ret != PPA_RET_SUCCESS) {
            std::cerr << "ppa_getPredictData Filed! ret:" << ppa_ret << std::endl;
            netaccess.Exit();
            spmNetInfoExit_r(&nif_inst); 
            ppa_disposeHandle(&ppa_handle);
            return false;
        }
        if (data.is_valid == 0) {
            netaccess.Exit();
            spmNetInfoExit_r(&nif_inst);
            ppa_disposeHandle(&ppa_handle);
            return false;
        }
        input->ppd_speed_list.emplace_back(data.spd);
    }
    netaccess.Exit();
    spmNetInfoExit_r(&nif_inst);

    //デバッグ用CSV出力
    // std::ofstream output_csv("speed_ppd.csv");
    // for (const auto& speed : input->ppd_speed_list) {
    //     output_csv << speed << std::endl;
    // }

    // 下限値を取得
    ppa_input.time = basetime_;
    PPA_DATA data = {0};
    ppa_ret = ppa_getPredictData(ppa_handle, &ppa_input, &data); 
    input->min_speed = static_cast<uint32_t>(data.lower);
    //lowerがうまく取得できないので一旦固定値(10km/h)をいれる
    //input->min_speed = 270;

    ppa_ret = ppa_disposeHandle(&ppa_handle);

    // std::vector<uint16_t> tmp;
    // std::copy(speed_list.begin(), speed_list.end(), std::back_inserter(tmp));
    // std::sort(tmp.begin(), tmp.end());
    input->standard_speed = *std::max_element(input->ppd_speed_list.begin(), input->ppd_speed_list.end());
    double hermonic_mean = util::CalcHarmonicMean(input->ppd_speed_list);
    time_t start_jam_time = 0;
    time_t end_jam_time = 0;
    uint32_t min_speed = UINT32_MAX;
    time_t peak_time = 0;
    time_t target_time = today_time_list_[0];
    // 平均以下の場合の時間帯を取得する
    for(const auto& data : input->ppd_speed_list) {
        if (data <= hermonic_mean) {
            if(data < min_speed) {
                min_speed = data;
                peak_time = target_time;
            }
            if (start_jam_time == 0) {
                start_jam_time = target_time; 
            }
            else {
                end_jam_time = target_time;
            }
        }    
        else {
            // 渋滞時間が1時間以上続くもののみ抽出
            if (start_jam_time != 0 && end_jam_time != 0 && (end_jam_time - start_jam_time) >= 3600) {
                input->jam_period_list.emplace_back(start_jam_time, end_jam_time);
                input->peak_speed.emplace_back(min_speed);
                input->jam_peak_time_list.emplace_back(peak_time);
            }
            min_speed = UINT32_MAX;
            peak_time = 0;
            start_jam_time = 0;
            end_jam_time = 0;
        }
        target_time += kTimeSpan;
    }
    // 収束時間が決まっていない場合(深夜帯に渋滞期間が存在する場合)
    if (start_jam_time != 0 && end_jam_time == 0) {
        input->jam_period_list.emplace_back(start_jam_time, target_time);
    }
    if (input->ppd_speed_list.empty() || input->jam_peak_time_list.empty() || input->jam_period_list.empty() || input->min_speed == 0) {
        return false;
    }

    return true;
}

// bool VICSShortPrediction::ComplementSpeed(const uint32_t& current_speed, const InputData& input, std::vector<SPEED>* speed_list) const {
//     //渋滞時を頂点とする、現在を通る二次曲線 y=a(x-p)^2+qを求める
//     // y = a(x - ppd_time)^2 + speed
//     //渋滞時間によって条件分岐(渋滞時間内ではない場合は何もしない、ピーク時間と現在時刻を比較して発生側か収束側か判断する)
//     int32_t period_index = -1;
//     for (int i = 0; i < input.jam_period_list.size(); ++i) {
//         if (input.jam_period_list[i].first <= basetime_ && input.jam_period_list[i].second >= basetime_) {
//             period_index = i;
//             break;
//         }
//     }
//     if (period_index == -1) {
//         return false;
//     }
//     // 速度リストをファイル数で初期化
//     speed_list->reserve(kMaxFileCount);
//     //渋滞時を頂点とする、現在を通る二次曲線 y=a(x-p)^2+qを求める
//     double before_a = (current_speed - input.min_speed) / std::pow((basetime_ - input.jam_peak_time_list[period_index]), 2);
//     //渋滞時を頂点とする、渋滞解消を通る二次曲線 y=a(x-p)^2+qを求める
//     double after_a = (input.standard_speed - input.min_speed) / std::pow((input.jam_period_list[period_index].second - input.jam_peak_time_list[period_index]), 2);
//     // ピーク時間までの速度を計算
//     time_t current_time = basetime_;
//     while (input.jam_peak_time_list[period_index] >= current_time + kTimeSpan) {
//         //30分ずつ進める
//         current_time += kTimeSpan;
//         uint32_t speed = before_a * std::pow((current_time - input.jam_peak_time_list[period_index]), 2) + input.min_speed;
//         TRAFFIC_LV traffic_lv = ti_func::GetTrafficLevel(input.link_attr, speed);
//         speed_list->emplace_back(speed, traffic_lv);
//     }
//     // ピーク時間から解消時間までの速度を計算
//     while (input.jam_period_list[period_index].second >= current_time + kTimeSpan) {
//         //30分ずつ進める
//         current_time += kTimeSpan;
//         uint32_t speed = after_a * std::pow((current_time - input.jam_peak_time_list[period_index]), 2) + input.min_speed;
//         TRAFFIC_LV traffic_lv = ti_func::GetTrafficLevel(input.link_attr, speed);
//         speed_list->emplace_back(speed, traffic_lv);
//     }
//     return true;
// }

bool VICSShortPrediction::ComplementSpeed(const uint32_t& current_speed, const InputData& input, std::vector<SPEED>* speed_list) const {
    int32_t period_index = -1;
    for (int i = 0; i < input.jam_period_list.size(); ++i) {
        if (input.jam_period_list[i].first <= basetime_ && input.jam_period_list[i].second >= basetime_) {
            period_index = i;
            break;
        }
    }
    if (period_index == -1) {
        return false;
    }
    // 速度リストをファイル数で初期化
    speed_list->reserve(kMaxFileCount);
    time_t current_time = basetime_;

    uint32_t start_jam_index = base_time_tm_.tm_hour * (3600 / kTimeSpan) + base_time_tm_.tm_min / (kTimeSpan / 60);

    
    for (int i = start_jam_index; i < input.ppd_speed_list.size(); ++i) {
        // 渋滞収束時間までの予測をする
        if (current_time >= input.jam_period_list[period_index].second) {
            break;
        }
        uint32_t pred_speed = static_cast<double>(input.ppd_speed_list[i]) / static_cast<double>(input.peak_speed[period_index]) * input.min_speed;
        TRAFFIC_LV traffic_lv = ti_func::GetTrafficLevel(input.link_attr, pred_speed); 
        speed_list->emplace_back(pred_speed, traffic_lv);
        current_time += kTimeSpan;
    }
    return true;
}

/**
 * @brief　渡されたリンクが予測対象か判断する 
 * 
 * @param mesh 
 * @param link 
 * @param basetime 
 * @return bool
 */
bool VICSShortPrediction::IsPredictionTarget(const uint32_t& mesh,const uint32_t& link,const time_t& basetime) {
    return true;
}

/**
 * @brief link_managerを出力ファイル数だけ初期化する
 */
void VICSShortPrediction::InitLinkManager() {
    for (int i = 0; i < kMaxFileCount; ++i) {
        LinkManager link_manager;
        link_manager_map_.emplace(i, link_manager);
    }
}

void VICSShortPrediction::SetLinkManager(const uint32_t& mesh, const uint32_t& vics_link, const std::vector<SPEED>& speed_list) {
    for (int i = 0; i < link_manager_map_.size(); ++i) {
        if (speed_list[i].first == 0) {
            continue;
        }
        mainnet::format::VicsLinkInformation vics_link_info;
        vics_link_info.link.dwMesh = mesh;
        vics_link_info.link.cLayer = vics_link >> 14;
        vics_link_info.link.cDiv =
                (vics_link & 0x3000) >> 12;
        vics_link_info.link.wVLink = vics_link & 0xFFF;
        vics_link_info.speed = speed_list[i].first;
        vics_link_info.jam_lv = speed_list[i].second;
        vics_link_info.flag[kUseTravelSpeed] = 1;
        vics_link_info.flag[kUseJam] = 1;
        link_manager_map_[i].AddVICSLinkInfo(
                2, vics_link_info);
        link_manager_map_[i].AddMeshInfo(
                2, vics_link_info.link.dwMesh);
        
    }
}

bool VICSShortPrediction::SetMainnetPath(const std::string& mainnet_dir) {
    std::ifstream ifs(mainnet_dir + "/" + kMainnetHed, std::ios::binary);
    if (ifs.is_open() != true) {
        std::cerr << "Failed open mainnet.hed!" << std::endl;
        return false;
    }
    ifs.seekg(16, std::ios::beg);
    char filename[14];  // YYYYMMDDhhmmss
    ifs.read(filename, 14);

    struct tm tm;
    strptime(filename, "%Y%m%d%H%M%S", &tm);
    time_t latest_time = mktime(&tm);
    // 15分ごとにmainnetの時間を格納する
    struct tm current_tm = tm;
    for (int i = 0; i <= kTargetHour * 4; ++i) {
        strftime(filename, sizeof(filename), "%Y%m%d%H%M%S", &current_tm);
        mainnet_name_list_.emplace_back(filename);
        time_t unixtime = mktime(&current_tm);
        unixtime -= 15 * 60;
        localtime_r(&unixtime, &current_tm);
    }
    return true;
}