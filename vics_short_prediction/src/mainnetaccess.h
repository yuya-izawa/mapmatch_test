//*****************************************************************************
//
//!		@file		MainnetAccess.h
//!		@brief
//!メインネットアクセスライブラリの公開ヘッダファイル
//!		@author		糸賀 一也		NAVITIME JAPAN Co.,Ltd.
//!		@date		2012/04/26
//
//*****************************************************************************
#ifndef MAINNET_ACCESS_H_
#define MAINNET_ACCESS_H_

#include <cstdint>
#include <vector>

namespace tia {
namespace mformat {
class LinkKey;
}  // namespace mformat
}  // namespace tia

//-----------------------------------------------------------------------------
//	マクロ定義
//-----------------------------------------------------------------------------
//! 交通情報要素数
#define MNA_ELEM_NUM (13)

//-----------------------------------------------------------------------------
//	定数定義
//-----------------------------------------------------------------------------
const int32_t kHasNextInfo = 1;
const int32_t kHasNoNextInfo = 0;

//-----------------------------------------------------------------------------
//	型宣言
//-----------------------------------------------------------------------------
//! ライブラリハンドル型
typedef struct _MNA_INSTANCE* MNA_HANDLE;

//! リターンコード列挙型
typedef enum _MNA_RETURN_CODE {
    MNA_SUCCESS = 0,                     //!< 正常終了
    MNA_ERROR_ILLEGAL_INPUT_PARAMS = 1,  //!< エラー終了（引数が不正)
    MNA_ERROR_ILLEGAL_INPUT_DATA =
            2,  //!< エラー終了（メインネットファイルのデータが不正）
    MNA_ERROR_FILE_OPERATIONS = 3,  //!< エラー終了（ファイル操作失敗）
    MNA_ERROR_MEMORY_OPERATIONS = 4  //!< エラー終了（メモリ操作失敗）
} MNA_RETURN_CODE;

#pragma pack(push, 4)

//! 交通情報構造体型
typedef struct _MNA_TRAFFIC_INFO {
    uint32_t speed;                    //!< 速度[cm/s]
    uint32_t reg_start_time;           //!< 規制開始日時[s]
    uint32_t reg_end_time;             //!< 規制終了日時[s]
    uint16_t jam_start_pos;            //!< 渋滞開始位置[10m]
    uint16_t jam_end_pos;              //!< 渋滞終了位置[10m]
    uint16_t reg_start_pos;            //!< 規制開始位置[10m]
    uint16_t reg_end_pos;              //!< 規制終了位置[10m]
    uint8_t timeslot;                  //!< 規制時間帯種別
    uint8_t traffic_lv;                //!< 渋滞度
    uint8_t reg;                       //!< 規制内容
    uint8_t reg_detail;                //!< 規制内容詳細
    uint8_t cause;                     //!< 原因事象
    uint8_t cause_detail;              //!< 原因事象詳細
    uint8_t valid_flag[MNA_ELEM_NUM];  //!< 有効フラグ
} MNA_TRAFFIC_INFO;

//! 単位SAPA情報構造体型
typedef struct _MNA_UINT_SAPA_INFO {
    int32_t lat;            //!< 中心緯度
    int32_t lon;            //!< 中心経度
    uint8_t sapa_lv;        //!< 混雑状況
    uint8_t large_sapa_lv;  //!< 大型車向け混雑状況
    uint8_t dummy[2];       //!< 予約領域
} MNA_UNIT_SAPA_INFO;

#pragma pack(pop)

//-----------------------------------------------------------------------------
//	関数プロトタイプ宣言
//-----------------------------------------------------------------------------
#ifdef __cplusplus
extern "C" {
#endif  // __cplusplus

// バージョン取得関数
extern int32_t MNA_GetVersion(void);

// ハンドル生成関数
extern MNA_RETURN_CODE MNA_CreateHandle(const char* Path, MNA_HANDLE* Handle);

// ハンドル破棄関数
extern MNA_RETURN_CODE MNA_DestroyHandle(MNA_HANDLE* Handle);

// 交通情報取得関数
extern MNA_RETURN_CODE MNA_GetTrafficInfo(MNA_HANDLE Handle,
                                          const uint8_t SearchLv,
                                          const uint32_t MeshID,
                                          const uint16_t LinkID,
                                          MNA_TRAFFIC_INFO* TrafficInfo);

// SAPA情報数取得関数
extern MNA_RETURN_CODE MNA_GetSAPAInfoNum(MNA_HANDLE Handle,
                                          uint32_t* SAPAInfoNum);

// SAPA情報取得関数
extern MNA_RETURN_CODE MNA_GetSAPAInfo(MNA_HANDLE Handle,
                                       const uint32_t SAPAInfoNum,
                                       MNA_UNIT_SAPA_INFO* SAPAInfo);

// 規制有りMリンク検索関数
extern bool MNA_HasRegulatedMLink(MNA_HANDLE Handle,
                                  const tia::mformat::LinkKey& m_link);

extern int32_t MNA_HasNextInfo(MNA_HANDLE Handle, const uint8_t SearchLv);

extern MNA_RETURN_CODE MNA_NextInfo(MNA_HANDLE Handle, const uint8_t SearchLv,
                                    uint32_t* MeshID, uint16_t* LinkID,
                                    MNA_TRAFFIC_INFO* TrafficInfo);

#ifdef __cplusplus
};
#endif  // __cplusplus

#endif  // MAINNET_ACCESS_H_

//*****************************************************************************
//	end of file
//*****************************************************************************
