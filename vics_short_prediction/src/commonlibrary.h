#ifndef _COMMON_LIB_H_
#define _COMMON_LIB_H_

#include <cassert>
#include <cstdint>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

namespace common {
namespace mesh {

/**
 * @brief AssertSecondMeshDomain
 * @details
 *  ゼンリンの提供する範囲で二次メッシュの定義域の検査
 *  汎用交換25Kより決定
 * @param mesh 日本測地系二次メッシュ
 */
inline void AssertSecondMeshDomain(const uint32_t mesh) {
    static const auto kMinumumDomainOfZenrinSecondMesh = 303640;
    static const auto kMaximumDomainOfZenrinSecondMesh = 684930;
    assert(kMinumumDomainOfZenrinSecondMesh <= mesh &&
           mesh <= kMaximumDomainOfZenrinSecondMesh);
}

/**
 * @brief World系2次メッシュ番号へ変換します。
 * @param mesh 日本測地系二次メッシュ
 * @return 変換値
 */
inline uint32_t ToWorldSecondMesh(const uint32_t mesh) {
    AssertSecondMeshDomain(mesh);

    uint32_t my = static_cast<uint32_t>(mesh / 10000);
    my += 135;

    uint32_t mx = static_cast<uint32_t>((mesh / 100) % 100);
    mx += 100;

    uint32_t mm = static_cast<uint32_t>(mesh % 100);

    return (my * 100000 + mx * 100 + mm);
}

/**
 * @brief 日本測地系二次メッシュから圧縮メッシュを取得.
 *
 * @param mesh 日本測地系二次メッシュ
 *
 * @return 引数の二次メッシュを4分割した左下の圧縮メッシュ番号.
 */
inline uint32_t GetBmeshFrom2ndMesh(const uint32_t mesh) {
    AssertSecondMeshDomain(mesh);
    return (((mesh / 10000) % 100 + 135) << 23) +
           (((mesh / 100) % 100 + 100) << 14) +
           ((((mesh / 10) % 10) & 0x07) << 11) + ((((mesh) % 10) & 0x07) << 8);
}

}  // namespace mesh
}  // namespace common

#endif
