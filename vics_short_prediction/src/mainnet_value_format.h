#ifndef MAINNET_VALUE_FORMAT_H
#define MAINNET_VALUE_FORMAT_H

#include <cstdint>
#include <string>

namespace mainnet {
namespace value {

//! mainnetファイルヘッダー識別子
const static std::string header_identifier("VD");

//! mainnetファイルバージョン
const uint8_t version = 0x01;

}  // namespace value
}  // namespace mainnet
#endif  // MAINNET_VALUE_FORMAT_H
