#ifndef LINK_MANAGER_H
#define LINK_MANAGER_H

#include "mainnet_binary_format.h"
#include "mainnet_value_format.h"
#include "mainnet_writer.h"
#include "na2_wrapper.h"
#include "nni_wrapper.h"
#include "vics_access/vics_link.h"

class LinkManager {
   public:
    LinkManager();
    bool AddMeshInfo(const uint32_t link_lv, const uint32_t mesh_id);
    bool AddVICSLinkInfo(
            const uint32_t link_lv,
            const mainnet::format::VicsLinkInformation& link_info);
    bool AddVICSLinkInfo(const uint32_t link_lv, const uint32_t inte);
    bool CreateMeshInfo(const uint32_t link_lv);
    bool GetMeshInfo(
            const uint32_t link_lv,
            std::vector<mainnet::format::MeshInformation>* mesh_info) const;
    bool GetVICSLinkInfo(
            const uint32_t link_lv,
            std::vector<mainnet::format::VicsLinkInformation>* link_info) const;
    bool GenerateVICSLinkInfo(
            const std::string map_path, const uint32_t level,
            const std::vector<SecondMeshUnitInformation>& secondmesh_info);

   private:
    // map<mesh, link_count>
    std::map<uint32_t, uint32_t> mesh_lv_2_;
    std::map<uint32_t, uint32_t> mesh_lv_3_;
    std::map<uint32_t, uint32_t> mesh_lv_4_;
    std::vector<mainnet::format::MeshInformation> mesh_info_lv2_;
    std::vector<mainnet::format::MeshInformation> mesh_info_lv3_;
    std::vector<mainnet::format::MeshInformation> mesh_info_lv4_;
    std::vector<mainnet::format::VicsLinkInformation> link_info_lv2_;
    std::vector<mainnet::format::VicsLinkInformation> link_info_lv3_;
    std::vector<mainnet::format::VicsLinkInformation> link_info_lv4_;

    bool FindMeshInfo(const uint32_t Mesh,
                      const std::vector<SecondMeshUnitInformation>& vlink,
                      SecondMeshUnitInformation& result);
    bool FindVicsLinkInfo(const mainnet::format::VICS_LINK vl,
                          const SecondMeshUnitInformation& mesh_info,
                          mainnet::format::VicsLinkInformation& result);
    void CopyJam(const mainnet::format::VicsLinkInformation& src,
                 mainnet::format::VicsLinkInformation* dst);
    void CopyRegulationAndObstruction(
            const mainnet::format::VicsLinkInformation& src,
            mainnet::format::VicsLinkInformation* dst);
    uint8_t GetPriority(const uint8_t regulation, const uint8_t cause) const;
    void CompareVicsInfo(mainnet::format::VicsLinkInformation* org_info,
                         const mainnet::format::VicsLinkInformation& next_info,
                         const int32_t nw_level);
    bool GetMLinks(const mainnet::format::VICS_LINK& vlink,
                   const std::string& map_path, std::vector<MF_LINK>* mlinks);
    bool CNVJAM_GetLengthOfMFArray(const NA2_INST na2_inst,
                                   const std::vector<MF_LINK>& mlinks,
                                   uint32_t* dwTotalLength);
    static int32_t Comp(const void* n1, const void* n2);
};

#endif  // LINK_MANAGER_H