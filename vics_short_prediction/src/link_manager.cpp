#include "link_manager.hpp"
#include <algorithm>
#include <cassert>
#include <iostream>
#include <stdexcept>
#include <string.h>
#include <utility>
#include <vector>

using namespace mainnet::value::enable_flags;

LinkManager::LinkManager() {}

bool LinkManager::AddMeshInfo(const uint32_t link_lv, const uint32_t mesh) {
    if (link_lv < 2 || link_lv > 4) {
        std::cerr << "invalid link_lv:" << link_lv << std::endl;
        return false;
    }
    switch (link_lv) {
        case 2:
            if (mesh_lv_2_.find(mesh) != mesh_lv_2_.end()) {
                mesh_lv_2_[mesh] += 1;
            } else {
                mesh_lv_2_[mesh] = 1;
            }
            return true;
        case 3:
            if (mesh_lv_3_.find(mesh) != mesh_lv_3_.end()) {
                mesh_lv_3_[mesh] += 1;
            } else {
                mesh_lv_3_[mesh] = 1;
            }
            return true;
        case 4:
            if (mesh_lv_4_.find(mesh) != mesh_lv_4_.end()) {
                mesh_lv_4_[mesh] += 1;
            } else {
                mesh_lv_4_[mesh] = 1;
            }
            return true;
        default:
            return false;
    }
}

bool LinkManager::AddVICSLinkInfo(
        const uint32_t link_lv,
        const mainnet::format::VicsLinkInformation& link_info) {
    if (link_lv < 2 || link_lv > 4) {
        std::cerr << "invalid link_lv:" << link_lv << std::endl;
        return false;
    }
    auto it = link_info_lv2_.begin();
    switch (link_lv) {
        case 2:
            it = std::find(link_info_lv2_.begin(), link_info_lv2_.end(),
                           link_info);
            if (it != link_info_lv2_.end() && it->speed > link_info.speed) {
                it->speed = link_info.speed;
            } else {
                link_info_lv2_.emplace_back(link_info);
            }
            return true;
        case 3:
            it = std::find(link_info_lv3_.begin(), link_info_lv3_.end(),
                           link_info);
            if (it != link_info_lv3_.end() && it->speed > link_info.speed) {
                it->speed = link_info.speed;
            } else {
                link_info_lv3_.emplace_back(link_info);
            }
            return true;
        case 4:
            it = std::find(link_info_lv4_.begin(), link_info_lv4_.end(),
                           link_info);
            if (it != link_info_lv4_.end() && it->speed > link_info.speed) {
                it->speed = link_info.speed;
            } else {
                link_info_lv4_.emplace_back(link_info);
            }
            return true;
        default:
            return false;
    }
}

bool LinkManager::GetMeshInfo(
        const uint32_t link_lv,
        std::vector<mainnet::format::MeshInformation>* mesh_info) const {
    if (link_lv < 2 || link_lv > 4) {
        std::cerr << "invalid link_lv:" << link_lv << std::endl;
        return false;
    }
    switch (link_lv) {
        case 2:
            *mesh_info = mesh_info_lv2_;
            return true;
        case 3:
            *mesh_info = mesh_info_lv3_;
            return true;
        case 4:
            *mesh_info = mesh_info_lv4_;
            return true;
        default:
            return false;
    }
}

bool LinkManager::GetVICSLinkInfo(
        const uint32_t link_lv,
        std::vector<mainnet::format::VicsLinkInformation>* link_info) const {
    if (link_lv < 2 || link_lv > 4) {
        std::cerr << "invalid link_lv:" << link_lv << std::endl;
        return false;
    }
    switch (link_lv) {
        case 2:
            link_info->clear();
            link_info->resize(link_info_lv2_.size());
            std::copy(link_info_lv2_.begin(), link_info_lv2_.end(),
                      link_info->begin());
            return true;
        case 3:
            link_info->clear();
            link_info->resize(link_info_lv3_.size());
            std::copy(link_info_lv3_.begin(), link_info_lv3_.end(),
                      link_info->begin());
            return true;
        case 4:
            link_info->clear();
            link_info->resize(link_info_lv4_.size());
            std::copy(link_info_lv4_.begin(), link_info_lv4_.end(),
                      link_info->begin());
            return true;
        default:
            return false;
    }
}

bool LinkManager::CreateMeshInfo(const uint32_t link_lv) {
    if (link_lv < 2 || link_lv > 4) {
        std::cerr << "invalid link_lv:" << link_lv << std::endl;
        return false;
    }
    std::map<uint32_t, uint32_t> mesh_count;
    switch (link_lv) {
        case 2:
            mesh_count = mesh_lv_2_;
            for (const auto& mesh : mesh_count) {
                mainnet::format::MeshInformation mesh_info;
                mesh_info.mesh = mesh.first;
                mesh_info.size = mesh.second;
                mesh_info_lv2_.emplace_back(mesh_info);
            }
            break;
        case 3:
            mesh_count = mesh_lv_3_;
            for (const auto& mesh : mesh_count) {
                mainnet::format::MeshInformation mesh_info;
                mesh_info.mesh = mesh.first;
                mesh_info.size = mesh.second;
                mesh_info_lv3_.emplace_back(mesh_info);
            }
            break;
        case 4:
            mesh_count = mesh_lv_4_;
            for (const auto& mesh : mesh_count) {
                mainnet::format::MeshInformation mesh_info;
                mesh_info.mesh = mesh.first;
                mesh_info.size = mesh.second;
                mesh_info_lv4_.emplace_back(mesh_info);
            }

            break;
        default:
            return false;
    }
}
//=============================================================================
/*	@brief  2次メッシュ番号情報を検索する
 *	@param[in]	Mesh	メッシュ番号
 *	@param[in]	vlink	2次メッシュ番号情報
 *  @param[out] 2次メッシュ番号情報
 *	@return		BOOL値(true;成功、false：失敗)
 */
//=============================================================================
bool LinkManager::FindMeshInfo(
        const uint32_t Mesh,
        const std::vector<SecondMeshUnitInformation>& vlink,
        SecondMeshUnitInformation& result) {
    const auto& find = std::lower_bound(
            vlink.begin(), vlink.end(), Mesh,
            [](const SecondMeshUnitInformation& info, const uint32_t Mesh) {
                return info.MeshInfo.mesh < Mesh;
            });
    if (find == vlink.end()) {
        return false;
    }
    if (find->MeshInfo.mesh != Mesh) {
        return false;
    }
    result = *find;
    return true;
}

//=============================================================================
/**
 *  @brief VICSリンク情報を検索する
 *
 *  @param[in]  vl  VICSリンク
 *  @param[in]  mesh_info   2次メッシュ番号情報
 *  @param[out] VICSリンク情報
 *
 *  @return BOOL値(true;成功、false：失敗)
 */
//=============================================================================
bool LinkManager::FindVicsLinkInfo(
        const mainnet::format::VICS_LINK vl,
        const SecondMeshUnitInformation& mesh_info,
        mainnet::format::VicsLinkInformation& result) {
    // 範囲外は最初にはじく
    if (mesh_info.MeshInfo.size == 0) {
        return false;
    }

    // VICSリンクIDで検索
    const auto vics_link_id = vl.GetCombinedVicsId();
    const auto& find = std::lower_bound(
            mesh_info.VicsLinkInfo.begin(), mesh_info.VicsLinkInfo.end(),
            vics_link_id,
            [](const mainnet::format::VicsLinkInformation& a,
               const uint16_t& vics_link_id) {
                return a.link.GetCombinedVicsId() < vics_link_id;
            });
    if (find == mesh_info.VicsLinkInfo.end()) {
        return false;
    }
    if (find->link != vl) {
        return false;
    }
    result = *find;
    return true;
}

void LinkManager::CopyJam(const mainnet::format::VicsLinkInformation& src,
                          mainnet::format::VicsLinkInformation* dst) {
    // input check
    assert(dst != nullptr);

    dst->flag[kUseJam] = src.flag[kUseJam];
    dst->jam_lv = src.jam_lv;
    dst->flag[kUseTravelSpeed] = src.flag[kUseTravelSpeed];
    dst->speed = src.speed;
    dst->jam_start_pos = src.jam_start_pos;
    dst->jam_end_pos = src.jam_end_pos;

    return;
}

void LinkManager::CopyRegulationAndObstruction(
        const mainnet::format::VicsLinkInformation& src,
        mainnet::format::VicsLinkInformation* dst) {
    // input check
    assert(dst != nullptr);

    dst->flag[kUseRegulation] = src.flag[kUseRegulation];
    dst->reg = src.reg;
    dst->flag[kUseRegDetail] = src.flag[kUseRegDetail];
    dst->detail_reg = src.detail_reg;
    dst->flag[kUseCause] = src.flag[kUseCause];
    dst->cause = src.cause;
    dst->flag[kUseCauseDetail] = src.flag[kUseCauseDetail];
    dst->detail_cause = src.detail_cause;
    dst->flag[kTimeSlotIntermit] = src.flag[kTimeSlotIntermit];
    dst->flag[kUseStartTime] = src.flag[kUseStartTime];
    dst->reg_start_time = src.reg_start_time;
    dst->flag[kUseEndTime] = src.flag[kUseEndTime];
    dst->reg_end_time = src.reg_end_time;
    dst->reg_start_pos = src.reg_start_pos;
    dst->reg_end_pos = src.reg_end_pos;

    return;
}

uint8_t LinkManager::GetPriority(const uint8_t regulation,
                                 const uint8_t cause) const {
    switch (regulation) {
        case 0: {
            switch (cause) {
                case 0: {
                    return 14;
                }
                case 1: {
                    return 6;
                }
                case 2: {
                    return 15;
                }
                case 3: {
                    return 7;
                }
                case 4: {
                    return 8;
                }
                case 5: {
                    return 16;
                }
                case 6: {
                    return 17;
                }
                case 7: {
                    return 18;
                }
                case 8: {
                    return 19;
                }
                case 9: {
                    return 20;
                }
                case 10: {
                    return 21;
                }
                case 14: {
                    return 22;
                }
                case 15: {
                    return 24;
                }
                default: {
                    return 255;
                }
            }
        }
        case 1: {
            return 0;
        }
        case 2: {
            return 1;
        }
        case 3: {
            return 9;
        }
        case 4: {
            return 10;
        }
        case 5: {
            return 11;
        }
        case 6: {
            return 5;
        }
        case 7: {
            return 2;
        }
        case 8: {
            return 4;
        }
        case 9: {
            return 12;
        }
        case 10: {
            return 3;
        }
        case 14: {
            return 13;
        }
        case 15: {
            return 23;
        }
        default: {
            return 255;
        }
    }
}

//=============================================================================
/**
 * @brief VICSリンク単位の情報を比較して優先度順に並替えをする
 *
 * @param[in,out] org_info 現在のVICSリンク情報
 * @param[in] next_info 次のVICSリンク情報
 * @param[in] vlink_lv Hierarchizated Network Level
 *
 * @return none
 */
//=============================================================================
void LinkManager::CompareVicsInfo(
        mainnet::format::VicsLinkInformation* org_info,
        const mainnet::format::VicsLinkInformation& next_info,
        const int32_t nw_level) {
    const uint8_t kFalse = 0;
    const uint8_t kTrue = 1;
    const uint8_t kInvalidJamLevel = 0;
    const uint32_t kInvalidSpeed = 0x1FFF;
    const uint16_t kInvalidPosition = 0xFFFF;

    // input check
    assert(org_info != nullptr);

    assert(org_info->link.dwMesh >= 362257 && org_info->link.dwMesh <= 684210);
    assert(org_info->link.cLayer == 2);
    assert(org_info->link.cDiv <= 3);
    assert(org_info->link.wVLink != 0);
    assert(next_info.link.dwMesh >= 362257 && next_info.link.dwMesh <= 684210);
    assert(next_info.link.cLayer == 2);
    assert(next_info.link.cDiv <= 3);
    assert(next_info.link.wVLink != 0);

    // NOTE:渋滞情報は同じリンク同士の場合のみマージ処理を実施する
    if (org_info->link == next_info.link) {
        // merge traffic jam
        if (!org_info->flag[kUseJam]) {
            assert(!org_info->flag[kUseTravelSpeed] &&
                   org_info->jam_start_pos == kInvalidPosition &&
                   org_info->jam_end_pos == kInvalidPosition);
        }

        if (!next_info.flag[kUseJam]) {
            assert(!next_info.flag[kUseTravelSpeed] &&
                   next_info.jam_start_pos == kInvalidPosition &&
                   next_info.jam_end_pos == kInvalidPosition);
        }

        if (!org_info->flag[kUseJam]) {
            if (next_info.flag[kUseJam]) {
                CopyJam(next_info, org_info);
            }
        } else if (org_info->jam_lv == kInvalidJamLevel) {
            if (next_info.flag[kUseJam]) {
                org_info->flag[kUseJam] = next_info.flag[kUseJam];
                org_info->jam_lv = next_info.jam_lv;
                org_info->jam_start_pos = next_info.jam_start_pos;
                org_info->jam_end_pos = next_info.jam_end_pos;

                if (!org_info->flag[kUseTravelSpeed]) {
                    if (next_info.flag[kUseTravelSpeed] &&
                        next_info.speed != kInvalidSpeed) {
                        org_info->flag[kUseTravelSpeed] =
                                next_info.flag[kUseTravelSpeed];
                        org_info->speed = next_info.speed;
                    }
                } else if (org_info->speed == kInvalidSpeed) {
                    if (next_info.flag[kUseTravelSpeed] &&
                        next_info.speed != kInvalidSpeed) {
                        org_info->flag[kUseTravelSpeed] =
                                next_info.flag[kUseTravelSpeed];
                        org_info->speed = next_info.speed;
                    } else {
                        org_info->flag[kUseTravelSpeed] = kFalse;
                        org_info->speed = kInvalidSpeed;
                    }
                } else {
                    if (next_info.flag[kUseTravelSpeed] &&
                        next_info.speed != kInvalidSpeed) {
                        org_info->flag[kUseTravelSpeed] = kTrue;
                        org_info->speed = (org_info->speed < next_info.speed)
                                                  ? next_info.speed
                                                  : org_info->speed;
                    }
                }
            }
        } else {
            if (next_info.flag[kUseJam]) {
                if (next_info.jam_lv != kInvalidJamLevel) {
                    org_info->flag[kUseJam] = kTrue;
                    org_info->jam_lv = (org_info->jam_lv > next_info.jam_lv)
                                               ? next_info.jam_lv
                                               : org_info->jam_lv;
                    org_info->jam_start_pos = next_info.jam_start_pos;
                    org_info->jam_end_pos = next_info.jam_end_pos;
                }

                if (!org_info->flag[kUseTravelSpeed]) {
                    if (next_info.flag[kUseTravelSpeed] &&
                        next_info.speed != kInvalidSpeed) {
                        org_info->flag[kUseTravelSpeed] =
                                next_info.flag[kUseTravelSpeed];
                        org_info->speed = next_info.speed;
                    }
                } else if (org_info->speed == kInvalidSpeed) {
                    if (next_info.flag[kUseTravelSpeed] &&
                        next_info.speed != kInvalidSpeed) {
                        org_info->flag[kUseTravelSpeed] =
                                next_info.flag[kUseTravelSpeed];
                        org_info->speed = next_info.speed;
                    } else {
                        org_info->flag[kUseTravelSpeed] = kFalse;
                        org_info->speed = kInvalidSpeed;
                    }
                } else {
                    if (next_info.flag[kUseTravelSpeed] &&
                        next_info.speed != kInvalidSpeed) {
                        org_info->flag[kUseTravelSpeed] = kTrue;
                        org_info->speed = (org_info->speed < next_info.speed)
                                                  ? next_info.speed
                                                  : org_info->speed;
                    }
                }
            }
        }

        if (nw_level != 2) {
            org_info->jam_start_pos = kInvalidPosition;
            org_info->jam_end_pos = kInvalidPosition;
        }
    }

    // NOTE:規制情報は子リンクの情報から重要性の高い情報を高階層リンクに適用する
    // merge traffic regulation
    assert(org_info->flag[kUseRegulation] == org_info->flag[kUseCause]);
    if (org_info->flag[kUseRegulation] && org_info->flag[kUseCause]) {
        assert(!((org_info->reg == 0 || org_info->reg == 15) &&
                 (org_info->cause == 0 || org_info->cause == 15)));
    }

    assert(next_info.flag[kUseRegulation] == next_info.flag[kUseCause]);
    if (next_info.flag[kUseRegulation] && next_info.flag[kUseCause]) {
        assert(!((next_info.reg == 0 || next_info.reg == 15) &&
                 (next_info.cause == 0 || next_info.cause == 15)));
    }

    typedef enum { kInvalid = 0, kRegulation, kObstruction } info_type;

    const info_type org_info_type =
            (!org_info->flag[kUseRegulation] && !org_info->flag[kUseCause])
                    ? kInvalid
            : (org_info->reg != 0) ? kRegulation
                                   : kObstruction;
    const info_type next_info_type =
            (!next_info.flag[kUseRegulation] && !next_info.flag[kUseCause])
                    ? kInvalid
            : (next_info.reg != 0) ? kRegulation
                                   : kObstruction;

    if (org_info_type == kInvalid) {
        if (next_info_type != kInvalid) {
            CopyRegulationAndObstruction(next_info, org_info);
        }
    } else {
        if (next_info_type != kInvalid) {
            const uint8_t org_info_priority =
                    GetPriority(org_info->reg, org_info->cause);
            assert(org_info_priority != 255);
            const uint8_t next_info_priority =
                    GetPriority(next_info.reg, next_info.cause);
            assert(next_info_priority != 255);
            if (org_info_priority > next_info_priority) {
                CopyRegulationAndObstruction(next_info, org_info);
            }
        }
    }

    if (nw_level != 2) {
        org_info->reg_start_pos = kInvalidPosition;
        org_info->reg_end_pos = kInvalidPosition;
    }

    return;
}

bool LinkManager::GetMLinks(const mainnet::format::VICS_LINK& vlink,
                            const std::string& map_path,
                            std::vector<MF_LINK>* mlinks) {
    assert(mlinks != nullptr);

    const VicsLink vics_link(vlink.dwMesh, vlink.GetCombinedVicsId());
    dao::NniWrapper nni;
    nni.Initialize(map_path);
    try {
        std::vector<dao::MformatLink> mlinks_tmp =
                nni.GetMformatLinkFromVicsLinkId(vics_link);

        for (const auto& m : mlinks_tmp) {
            mlinks->emplace_back(m.GetMeshIndex(), m.GetLinkIndex());
        }

    } catch (const std::exception& e) {
        std::cerr << e.what() << std::endl;
        return false;
    }

    return true;
}

//=============================================================================
/**
 *	@brief	VICSリンクのMFリンク列からVICSリンクの総距離を求める
 *
 *	@param[in]	jm_detail		cnvjam用データ構造体
 *	@param[in]	mlinks			M-formatリンク列
 *	@param[in]	dwTotalLength	入力されたmlinksの全リンク長(m)
 *
 *	@return		ステータスコード(CNVJAM_SUCCESS)
 */
//=============================================================================
bool LinkManager::CNVJAM_GetLengthOfMFArray(const NA2_INST na2_inst,
                                            const std::vector<MF_LINK>& mlinks,
                                            uint32_t* dwTotalLength) {
    uint32_t ret = 0;
    uint32_t length = 0;
    NA2_LINK_ATTR_32_T l_attr;

    if (mlinks.empty()) {
        return false;
    }

    //リンク詳細を取得して各MFリンクの長さを取得
    for (const auto& mlink : mlinks) {
        ret = na2GetLinkAttr_32_r(na2_inst, mlink.dwMesh, mlink.dwLink,
                                  &l_attr);
        if (ret) {
            return false;
        }
        length = l_attr.a2.distance;
        *dwTotalLength += length;
    }

    return ret;
}

/**
 *  @brief	VICS情報の並替え(VICSリンクID順)
 *  @param[in]  a   比較対象1
 *  @param[in]  b   比較対象2
 *  @return ステータスコード(CNVJAM_SUCCESS)
 */
int32_t LinkManager::Comp(const void* n1, const void* n2) {
    const auto* a = (mainnet::format::VicsLinkInformation*)n1;
    const auto* b = (mainnet::format::VicsLinkInformation*)n2;

    const auto a_id = a->link.GetCombinedVicsId();
    const auto b_id = b->link.GetCombinedVicsId();

    // VICSリンク番号->速度で比較する
    if (a->link.dwMesh < b->link.dwMesh) {
        return -1;
    } else if (a->link.dwMesh > b->link.dwMesh) {
        return 1;
    } else if (a_id < b_id) {
        return -1;
    } else if (a_id > b_id) {
        return 1;
    } else if (a->speed < b->speed) {
        return -1;
    } else if (a->speed > b->speed) {
        return 1;
    }

    return memcmp(a, b, sizeof(mainnet::format::VicsLinkInformation));
}

bool LinkManager::GenerateVICSLinkInfo(
        const std::string map_path, const uint32_t level,
        const std::vector<SecondMeshUnitInformation>& secondmesh_info) {
    const uint32_t secondmesh_num = secondmesh_info.size();

    //ファイル名を生成する。レベル3と4以外はエラーとする
    std::string filename;
    switch (level) {
        case 3:
            filename = map_path + "/hwcdata/Lv3_VicsLinkInfo.dat";
            break;
        case 4:
            filename = map_path + "/hwcdata/Lv4_VicsLinkInfo.dat";
            break;
        default:
            assert("Invalid level num");
    }

    // datファイルをオープンする
    std::ifstream ifs(filename.c_str(), std::ios_base::binary);
    if (ifs.fail()) {
        std::cerr << "Failed to open VicsLinkInfo.dat file !" << std::endl;
        return false;
    }

    //ファイルヘッダーを読み込む
    UNITED_HEADER united_header = {0};
    memset(&united_header, 0, sizeof(UNITED_HEADER));
    ifs.read(reinterpret_cast<char*>(&united_header), sizeof(UNITED_HEADER));
    if (ifs.fail()) {
        return false;
    }

    //ヘッダーを確認
    if (united_header.Level != level || united_header.Number < 1) {
        return false;
    }

    //リンクヘッダを読み込む
    std::vector<LINK_HEADER> link_header(united_header.Number);
    ifs.read(reinterpret_cast<char*>(&link_header.front()),
             sizeof(LINK_HEADER) * link_header.size());
    if (ifs.fail()) {
        return false;
    }

    //リンク情報を取り出す
    std::vector<mainnet::format::VicsLinkInformation> vlink_info(
            united_header.Number);
    for (uint32_t i = 0; i < united_header.Number; i++) {
        uint32_t total_len = 0;
        uint32_t total_time = 0;

        if (!link_header[i].Number) {
            continue;
        }
        //先頭のVICSリンク情報を取得する
        vlink_info[i].link.dwMesh = link_header[i].SecondMesh;
        vlink_info[i].link.SetVicsLinkId(link_header[i].VicsLink);
        vlink_info[i].jam_start_pos = 0xffff;
        vlink_info[i].jam_end_pos = 0xffff;
        vlink_info[i].reg_start_pos = 0xffff;
        vlink_info[i].reg_end_pos = 0xffff;

        //紐づくVICSリンク情報を取得する
        mainnet::format::VICS_LINK prev_vl = vlink_info[i].link;
        for (uint32_t j = 0; j < link_header[i].Number; j++) {
            VICS_LINK_INFO vics_link = {0};
            ifs.read(reinterpret_cast<char*>(&vics_link),
                     sizeof(VICS_LINK_INFO));
            if (ifs.fail()) {
                return false;
            }
            const mainnet::format::VICS_LINK vl(vics_link.SecondMesh,
                                                vics_link.VicsLink);

            bool fValid = true;
            if (j != 0) {
                if (vl == prev_vl) {
                    fValid = false;
                }
            }

            mainnet::format::VicsLinkInformation next_info;
            if (fValid) {
                SecondMeshUnitInformation second_info;
                if (FindMeshInfo(vl.dwMesh, secondmesh_info, second_info)) {
                    if (!FindVicsLinkInfo(vl, second_info, next_info)) {
                        fValid = false;
                    }
                } else {
                    fValid = false;
                }
            }

            //情報の比較を行う
            if (fValid) {
                CompareVicsInfo(&vlink_info[i], next_info, level);
                prev_vl = vl;

                //リンク旅行時間が有効なVICSリンクの総距離と通行時間を計測
                if (next_info.flag[FLAG_TRAVEL_SPEED] == true) {
                    std::vector<MF_LINK> mlinks;
                    if (GetMLinks(next_info.link, map_path, &mlinks)) {
                        uint32_t len = 0;
                        NA2_INST na2_inst;
                        uint32_t ret = na2InitNetAccess_32_r(
                                &na2_inst, NA2_MODE_NET_ACCESS_FULL,
                                map_path.c_str());
                        if (ret) {
                            std::cerr << "error! na2InitNetAccess_32_r:" << ret
                                      << std::endl;
                            return false;
                        }
                        CNVJAM_GetLengthOfMFArray(na2_inst, mlinks, &len);
                        na2ExitNetAccess_r(&na2_inst);
                        if (len != UINT32_MAX) {
                            len = len * 100;  // cm単位にする
                            if (len >= next_info.speed && next_info.speed > 0) {
                                total_len += len;
                                total_time += len / next_info.speed;
                            }
                        }
                    }
                }
            }
            if (vlink_info[i].flag[FLAG_TRAVEL_SPEED] == true &&
                j == link_header[i].Number - 1) {
                if (total_len >= total_time && total_len > 0 &&
                    total_time > 0) {
                    vlink_info[i].speed = total_len / total_time;
                } else {
                    vlink_info[i].speed = 1;
                }
            }
            // END:MOD
        }
    }

    //ソートする
    qsort(&vlink_info.front(), united_header.Number,
          sizeof(mainnet::format::VicsLinkInformation), Comp);

    //同じVICSリンクが存在する時は、情報をマージする
    for (uint32_t i = 1; i < united_header.Number; i++) {
        if (vlink_info[i - 1].link == vlink_info[i].link) {
            CompareVicsInfo(&vlink_info[i], vlink_info[i - 1], level);
        }
    }

    //マージされた情報は後ろに集約されているので、逆にループを回して同じリンクの時は上書きする
    for (uint32_t i = united_header.Number - 1; i > 0; i--) {
        if (vlink_info[i].link == vlink_info[i - 1].link) {
            vlink_info[i - 1] = vlink_info[i];
        }
    }

    //収録されている、メッシュ総数を計測
    uint32_t mesh_num = 1;
    for (uint32_t i = 1; i < united_header.Number; i++) {
        if (vlink_info[i - 1].link.dwMesh != vlink_info[i].link.dwMesh) {
            mesh_num++;
        }
    }
    //メッシュ情報構造体を作成
    std::vector<mainnet::format::MeshInformation> mesh_info(mesh_num);
    mesh_info[0].mesh = vlink_info[0].link.dwMesh;
    mesh_info[0].size = 1;
    mesh_num = 0;
    for (uint32_t i = 1; i < united_header.Number; i++) {
        if (mesh_info[mesh_num].mesh != vlink_info[i].link.dwMesh) {
            mesh_num++;
            mesh_info[mesh_num].mesh = vlink_info[i].link.dwMesh;
            mesh_info[mesh_num].size = 1;
        } else {
            mesh_info[mesh_num].size++;
        }
    }

    // 事故渋滞短期予測には渋滞のリンクのみの情報を入れる
    std::vector<mainnet::format::VicsLinkInformation> vlink_info_tmp;
    std::map<uint32_t, uint32_t> mesh_map;
    for (const auto& info : vlink_info) {
        if (info.flag[kUseJam] && info.flag[kUseTravelSpeed]) {
            vlink_info_tmp.emplace_back(info);
            if (mesh_map.find(info.link.dwMesh) == mesh_map.end()) {
                mesh_map[info.link.dwMesh] = 1;
            } else {
                mesh_map[info.link.dwMesh] += 1;
            }
        }
    }

    std::vector<mainnet::format::MeshInformation> mesh_info_tmp;
    for (const auto& mesh : mesh_map) {
        mainnet::format::MeshInformation info;
        info.mesh = mesh.first;
        info.size = mesh.second;
        mesh_info_tmp.emplace_back(info);
    }

    // vlink_info.resize(united_header.Number);
    // mesh_info.resize(mesh_num + 1);
    switch (level) {
        case 3:
            link_info_lv3_ = std::move(vlink_info_tmp);
            mesh_info_lv3_ = std::move(mesh_info_tmp);
            break;
        case 4:
            link_info_lv4_ = std::move(vlink_info_tmp);
            mesh_info_lv4_ = std::move(mesh_info_tmp);
            break;
        default:
            assert("Invalid level num");
    }

    return true;
}