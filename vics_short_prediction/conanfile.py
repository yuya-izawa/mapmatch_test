from conans import ConanFile


class AccidentJamShortPredictionConan(ConanFile):
    name = "vics_short_prediction"
    license = "Proprietary"
    url = "git@bitbucket.org:ntj-developer/vics_short_prediction.git"
    description = "vics_short_prediction"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = "shared=False"
    requires = (
        "probe_traffic_analysis/[1.0.x]@ntj/stable",
        "newnetinfo/[2.1.x]@ntj/stable",
        "tiaccess/[4.5.x]@ntj/testing_add_lower",
        "vics_access/[2.1.x]@ntj/stable",
        "ppdaccess/[1.3.0]@ntj/testing_add_lower",
    )
    build_requires = (
        "gtest/[1.x]@ntj/stable",
    )

    def imports(self):
        self.copy("*.h*", dst="externals/include", src="include")
        self.copy("*.a", dst="externals/lib/linux64", src="lib")
        self.copy("*.so*", dst="externals/lib/linux64", src="lib")
    
    def package(self):
        if self.settings.build_type == "Debug":
            self.copy("vics_short_predictiond", dst="bin", src="build/Debug", keep_path=False) 
        else:
            self.copy("vics_short_prediction", dst="bin", src="build/Release", keep_path=False) 
