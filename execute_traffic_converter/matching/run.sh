#!/bin/bash
set -x

function unsetCredentials() {
  unset AWS_ACCESS_KEY_ID
  unset AWS_SECRET_ACCESS_KEY
  unset AWS_SESSION_TOKEN
  unset AWS_REGION
}

function setCredentials() {
  unsetCredentials

  if [[ ${JOB_NAME} == */* ]]; then
    JOB_NAME=$(echo ${JOB_NAME} | awk -F'/' '{print $NF}')
  fi

  ROLE="arn:aws:iam::$1:role/$2"
  SESSION=`aws sts assume-role --role-arn ${ROLE}  --role-session-name "${JOB_NAME}" | jq -r '"\(.Credentials.AccessKeyId),\(.Credentials.SecretAccessKey),\(.Credentials.SessionToken)"'`
  export AWS_ACCESS_KEY_ID=`echo ${SESSION} | cut -d ',' -f 1`
  export AWS_SECRET_ACCESS_KEY=`echo ${SESSION} | cut -d ',' -f 2`
  export AWS_SESSION_TOKEN=`echo ${SESSION} | cut -d ',' -f 3`
  export AWS_REGION='ap-northeast-1'
}

function copy_map_data() {
  setCredentials 602094247784 "Release_Jenkins_route"

  aws s3 cp ${S3_MFORMAT_PATH}/mformat/daily_version/$(date -d ${TARGET_DATE} +%Y%m)/${TARGET_DATE}.txt .
  map_version=$(cat ${TARGET_DATE}.txt)

  mkdir map_data
  cd map_data

  # mformat
  aws s3 cp ${S3_MFORMAT_PATH}/mformat/${map_version}/fna_mobile_noadd.tgz ./
  tar -xzvf fna_mobile_noadd.tgz
  rm fna_mobile_noadd.tgz
  mv gef/netdata netdata
  mv gef/fna fna

  # hwcdata
  aws s3 cp ${S3_MFORMAT_PATH}/hwcdata/${map_version}/hwcdata.tgz ./
  tar -xzvf hwcdata.tgz
  rm hwcdata.tgz

  cd ../
}

function download_navilog_files() {
  CONVERT_SERVICE=$(cat ${CONFIG_PATH}/servicetype.json | jq -c '[.ntj_service_type,.kddi_service_type]' | sed -e 's/,/|/g' -e 's/[]["]//g')
  CONVERT_DATETIME=`date -d "$CURRENTDATE" +"%Y%m%d%H%M"`00

  setCredentials 602094247784 "Release_Jenkins_route"

  NAVILOG_TARGET_LOW=0
  for MINUTE in $(seq 1 $TARGET_MINUTE)
  do
      TARGET_DATETIME=$(date -d "${CURRENTDATE} ${MINUTE} minute ago" +"%Y%m%d%H%M")
      # ファイルの存在チェック　前回の実行分のチェックはNTJの方で確認
      ls ${NAVILOG_PATH_NTJ}/tracking_${TARGET_DATETIME}*.csv > /dev/null 2>&1
      if [ $? -ne 0 ]; then
          # ダウンロード済みファイル内になければ, s3から取得する
          NAVILOG_TARGET_DATE=$(date -d "${CURRENTDATE} ${MINUTE} minute ago" +"%Y%m%d")
          # 精度検証するだけなら, 公式のみで良い
          aws s3 cp ${S3_NAVILOG_PATH}/ntj/${NAVILOG_TARGET_DATE}/ ${NAVILOG_PATH_NTJ} --recursive --exclude "*" --include "tracking_${TARGET_DATETIME}*.csv.gz"
          # aws s3 cp ${S3_NAVILOG_PATH}/kddi/${NAVILOG_TARGET_DATE}/ ${NAVILOG_PATH_KDDI} --recursive --exclude "*" --include "tracking_${TARGET_DATETIME}*.csv.gz"
          aws s3 cp ${S3_NAVILOG_PATH}/asp/${NAVILOG_TARGET_DATE}/ ${NAVILOG_PATH_NTJ} --recursive --exclude "*" --include "tracking_${TARGET_DATETIME}*.csv.gz"

          zcat ${NAVILOG_PATH_NTJ}/tracking_${TARGET_DATETIME}*.csv.gz | egrep ${CONVERT_SERVICE} >> ${NAVILOG_PATH_NTJ}/tracking_${TARGET_DATETIME}00.csv
          # zcat ${NAVILOG_PATH_KDDI}/tracking_${TARGET_DATETIME}*.csv.gz | egrep ${CONVERT_SERVICE} >> ${NAVILOG_PATH_KDDI}/tracking_${TARGET_DATETIME}00.csv 
          rm ${NAVILOG_PATH_NTJ}/tracking_${TARGET_DATETIME}*.csv.gz
          # rm ${NAVILOG_PATH_KDDI}/tracking_${TARGET_DATETIME}*.csv.gz
      fi

      NAVILOG_LOW=$(wc -l ${NAVILOG_PATH_NTJ}/tracking_${TARGET_DATETIME}00.csv | awk '{print $1}')
      NAVILOG_TARGET_LOW=$((${NAVILOG_TARGET_LOW}+${NAVILOG_LOW}))
      if [ ${NAVILOG_TARGET_LOW} -ge ${NAVILOG_MAX_LOW} ]; then
        break
      fi
  done

  echo "[INFO] target minute: ${MINUTE}" >> matching_${TARGET_DATE}.log
  echo "[INFO] target lows: ${NAVILOG_TARGET_LOW}" >> matching_${TARGET_DATE}.log
}

function setup() {
  source ./set_env.sh

  conan install . -s build_type=Release
  chmod 755 ${BIN_PATH}/matching_converter
  export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${BIN_PATH}
  
  copy_map_data

  mkdir -p ${NAVILOG_PATH_NTJ} | true
  mkdir output | true
}

function upload_artifact() {
  setCredentials 602094247784 "Release_Jenkins_route"

  aws s3 cp --recursive output/ ${S3_UPLOAD_PATH}/${TARGET_DATE}/
  aws s3 cp matching_${TARGET_DATE}.log ${S3_UPLOAD_PATH}/${TARGET_DATE}/
}

function main() {
  TARGET_DATE=$1
  TARGET_MINUTE=$2  # マッチングに使うナビログの参照時間

  TARGET_TIME=$(date -d "${TARGET_DATE}" +"%Y/%m/%d %H:%M:")00
  END_DATETIME=$(date -d "${TARGET_TIME} 1 day" +"%Y/%m/%d %H:%M:%S")  # 終了判定に利用
  CURRENTDATE=$TARGET_TIME  # ループ用変数

  setup

  while [ 1 ] ; do
    convert_start_time=$(date +%s)
    echo "[INFO] ${CURRENTDATE}: convert start." >> matching_${TARGET_DATE}.log

    start_time=$(date +%s)
    download_navilog_files
    end_time=$(date +%s)
    echo "[INFO] $((end_time - start_time)) [s] for download_navilog_files." >> matching_${TARGET_DATE}.log

    source ${BASE_PATH}/make_config.sh NTJ ${CONVERT_DATETIME}

    start_time=$(date +%s)
    ${BIN_PATH}/matching_converter ${CONFIG_PATH}/config_ntj.json $(date -d "$CURRENTDATE" +"%s")
    if [ $? -ne 0 ]; then
        # convert error
        exit 1
    fi
    end_time=$(date +%s)
    echo "[INFO] $((end_time - start_time)) [s] for matching_converter." >> matching_${TARGET_DATE}.log

    gzip -f ${COSTDATA_PATH}/${CONVERT_DATETIME}_ntj

    convert_end_time=$(date +%s)
    echo "[INFO] $((convert_end_time - convert_start_time)) [s] for convert." >> matching_${TARGET_DATE}.log

    CURRENTDATE=$(date -d "${CURRENTDATE} ${CONVERT_MINUTE} minutes" +"%Y/%m/%d %H:%M:%S")
    if [[ $CURRENTDATE = $END_DATETIME ]] ; then
        break
    fi

    # 古い一次ナビログの削除
    find ${NAVILOG_PATH_NTJ}/ -mmin +${REMOVE_NAVILOG_TIME} | xargs --no-run-if-empty rm

  done

  upload_artifact
}

main "$@"
