readonly BASE_PATH=.
readonly CONFIG_PATH=${BASE_PATH}/config
readonly BIN_PATH=${BASE_PATH}/bin
readonly NAVILOG_PATH_NTJ=${BASE_PATH}/gpslog/ntj
# readonly NAVILOG_PATH_KDDI=${BASE_PATH}/gpslog/kddi
readonly MAP_PATH=${BASE_PATH}/map_data
readonly INTERSECTION_DATA_PATH=${MAP_PATH}/hwcdata/intersection_link.tsv
readonly COSTDATA_PATH=${BASE_PATH}/output

readonly TARGET_MINUTE=${TARGET_MINUTE}
readonly REMOVE_NAVILOG_TIME=120 # 指定した以前のナビログファイルを削除
readonly CONVERT_MINUTE=5  # コンバート間隔
# ナビログの最大参照行数. C5.2xlargeでの処理がこれくらいで限界っぽい
readonly NAVILOG_MAX_LOW=20000000

readonly S3_NAVILOG_PATH="s3://ntj-navilog-csv-traffic-info-pj"
readonly S3_MFORMAT_PATH="s3://ntj-common-mformat/car"
readonly S3_UPLOAD_PATH="s3://ntj-acts-route/TrafficInfoEvaluation/TrafficInfo/TRAFFIC-3255/costdata_max${TARGET_MINUTE}m"
