#!/bin/bash
# ##################################################################
# description:
#  - Making matchig converter config
# ##################################################################
SERVICE_TYPE=$1
case ${SERVICE_TYPE} in
#公式
"NTJ" )    
    echo '{' > ${CONFIG_PATH}/config_ntj.json
    {
        echo '"MapPath": "'${MAP_PATH}'",'
        echo '"IntersectionPath": "'${INTERSECTION_DATA_PATH}'",'
        echo '"NavilogDirectory": "'${NAVILOG_PATH_NTJ}'",'
        echo '"OutputPath": "'${COSTDATA_PATH}/${CONVERT_DATETIME}_ntj'",'
        echo '"TargetMinute": "'${MINUTE}'"' 
        echo }
    } >> ${CONFIG_PATH}/config_ntj.json
    ;;
"KDDI" )
    echo '{' > ${CONFIG_PATH}/config_kddi.json
    {
        echo '"MapPath": "'${MAP_PATH}'",'
        echo '"IntersectionPath": "'${INTERSECTION_DATA_PATH}'",'
        echo '"NavilogDirectory": "'${NAVILOG_PATH_KDDI}'",'
        echo '"OutputPath": "'${COSTDATA_PATH}/${CONVERT_DATETIME}_kddi'",'
        echo '"TargetMinute": "'${MINUTE}'"' 
        echo }
    } >> ${CONFIG_PATH}/config_kddi.json
    ;;
*)
    messages "[ERROR] make_config error" | ${SLACK_SEND} -t "${HOST} : aws $1 costdata 更新スクリプト" -i :ng_stamp: -n ${BOT_NAME} -l "#C2293B" -c ${CHANNEL_ALERT}
esac
  


