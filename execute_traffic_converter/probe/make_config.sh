#!/bin/bash
# ##################################################################
# description:
#  - Making probe realtime config for aws
# ##################################################################

echo '{' > ${BASE_PATH}/config.json
{
    echo '"LinkCostPath": "'${INPUT_PATH}'",'
    echo '"MapPath": "'${MAP_PATH}'",'
    echo '"MtinetDir": "'${OUTPUT_PATH}'",'
    echo '"MtinetFile": "'${CONVERT_DATETIME}'",'
    echo '"LaneCost": "'${LANE_COST_PATH}'/'${CONVERT_DATETIME}'.csv",'
    echo '"TrafficShape": "'${TRAFFIC_SHAPE_PATH}/${CONVERT_DATETIME}'.csv",'
    echo '"ConvertDatetimeStr": "'${ConvertDatetimeStr}'"'
    echo }
} >> ${BASE_PATH}/config.json
