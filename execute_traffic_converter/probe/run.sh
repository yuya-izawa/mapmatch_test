#!/bin/bash
set -ex

function unsetCredentials() {
  unset AWS_ACCESS_KEY_ID
  unset AWS_SECRET_ACCESS_KEY
  unset AWS_SESSION_TOKEN
  unset AWS_REGION
}

function setCredentials() {
  unsetCredentials

  if [[ ${JOB_NAME} == */* ]]; then
    JOB_NAME=$(echo ${JOB_NAME} | awk -F'/' '{print $NF}')
  fi

  ROLE="arn:aws:iam::$1:role/$2"
  SESSION=`aws sts assume-role --role-arn ${ROLE}  --role-session-name "${JOB_NAME}" | jq -r '"\(.Credentials.AccessKeyId),\(.Credentials.SecretAccessKey),\(.Credentials.SessionToken)"'`
  export AWS_ACCESS_KEY_ID=`echo ${SESSION} | cut -d ',' -f 1`
  export AWS_SECRET_ACCESS_KEY=`echo ${SESSION} | cut -d ',' -f 2`
  export AWS_SESSION_TOKEN=`echo ${SESSION} | cut -d ',' -f 3`
  export AWS_REGION='ap-northeast-1'
}

function copy_map_data() {
  setCredentials 602094247784 "Release_Jenkins_route"

  aws s3 cp ${S3_MFORMAT_PATH}/mformat/daily_version/$(date -d ${TARGET_DATE} +%Y%m)/${TARGET_DATE}.txt .
  map_version=$(cat ${TARGET_DATE}.txt)

  mkdir map_data
  cd map_data

  # mformat
  aws s3 cp ${S3_MFORMAT_PATH}/mformat/${map_version}/fna_mobile_noadd.tgz ./
  tar -xzvf fna_mobile_noadd.tgz
  rm fna_mobile_noadd.tgz
  mv gef/netdata netdata
  mv gef/fna fna

  # hwcdata
  aws s3 cp ${S3_MFORMAT_PATH}/hwcdata/${map_version}/hwcdata.tgz ./
  tar -xzvf hwcdata.tgz
  rm hwcdata.tgz

  cd ../
}

function setup() {
  source ./set_env.sh

  conan install . -s build_type=Release
  chmod 755 ${BIN_PATH}/probe_converter
  export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${BIN_PATH}
  
  copy_map_data

  mkdir input | true
  mkdir output | true
  mkdir lane_cost | true
  mkdir traffic_shape | true

  aws s3 cp --recursive ${S3_COSTDATA_PATH}/${TARGET_DATE}/ costdata/
  gunzip costdata/*.gz
}

function upload_artifact() {
  setCredentials 602094247784 "Release_Jenkins_route"

  aws s3 cp --recursive output/ ${S3_MTINET_UPLOAD_PATH}/${TARGET_DATE}/
  aws s3 cp --recursive lane_cost/ ${S3_LANE_COST_UPLOAD_PATH}/${TARGET_DATE}/
  
}

function main() {
  TARGET_DATE=$1

  TARGET_TIME=$(date -d ${TARGET_DATE} +"%Y/%m/%d %H:%M:")00
  END_DATETIME=$(date -d "${TARGET_TIME} 1 day" +"%Y/%m/%d %H:%M:%S")  # 終了判定に利用
  CURRENTDATE=$TARGET_TIME  # ループ用変数

  setup

  while [ 1 ] ; do
    CONVERT_DATETIME=$(date -d "$CURRENTDATE" +"%Y%m%d%H%M")00
    ConvertDatetimeStr=$(date -d "$CURRENTDATE" +"%Y-%m-%d %H:%M:%S")
    source ${BASE_PATH}/make_config.sh ${CONVERT_DATETIME} ${ConvertDatetimeStr}

    # costdata は input 配下すべてを読み込む仕様っぽいので, 毎回別ディレクトリへ mv し, コンバート後削除する
    mv costdata/${CONVERT_DATETIME}* ${INPUT_PATH}/
    ${BIN_PATH}/probe_converter ${BASE_PATH}/config.json
    rm -rf ${INPUT_PATH}/${CONVERT_DATETIME}*

    CURRENTDATE=$(date -d "${CURRENTDATE} ${CONVERT_MINUTE} minutes" +"%Y/%m/%d %H:%M:%S")
    if [[ $CURRENTDATE = $END_DATETIME ]] ; then
        break
    fi
  done

  upload_artifact
}

main "$@"