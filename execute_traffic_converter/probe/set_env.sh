readonly BASE_PATH=.
readonly INPUT_PATH=${BASE_PATH}/input
readonly BIN_PATH=${BASE_PATH}/bin
readonly MAP_PATH=${BASE_PATH}/map_data
readonly OUTPUT_PATH=${BASE_PATH}/output
readonly LANE_COST_PATH=${BASE_PATH}/lane_cost
readonly DATA_PATH=${BASE_PATH}/traffic_data
readonly TRAFFIC_SHAPE_PATH=${BASE_PATH}/traffic_shape

readonly CONVERT_MINUTE=5  # コンバート間隔
#早川さんが作成した、最大30分で動的にインプットを変更したものを使用する
readonly S3_COSTDATA_PATH="s3://ntj-acts-route/TrafficInfoEvaluation/TrafficInfo/TRAFFIC-3255/costdata_max30m"
readonly S3_MFORMAT_PATH="s3://ntj-common-mformat/car"
readonly S3_MTINET_UPLOAD_PATH="s3://ntj-acts-route/TrafficInfoEvaluation/TrafficInfo/TRAFFIC-3380/mainnet_probe_ex"
readonly S3_LANE_COST_UPLOAD_PATH="s3://ntj-acts-route/TrafficInfoEvaluation/TrafficInfo/TRAFFIC-3380/lane_cost"
